// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    API_KEY: '',
    API_URL: {
        // default1: 'http://192.168.0.104:9086/bvn/',
        default1: 'https://agencyservices.com.ng:9086/bvn/',
        default2: 'http://196.6.103.10:9086/bvn/',
        default3: 'http://197.255.63.146:9086/bvn/',
        default4: 'http://196.6.103.10:9086/bvn/',
        // default4: 'http://192.168.0.104:9086/bvn/',
        // default4: 'https://agencyservices.com.ng:9086/bvn/',
        default5: 'http://196.6.103.30:9086/bvn/',
        dashboard: 'dashboard',
        role: 'role',
        task: 'task',
        agt_mgr: 'user/agt-mgr',
        agent: 'agent',
        user: 'user',
        state: 'state',
        lga: 'lga',
        enroller: 'enroller',
        banks: 'banks',
        login: 'login',
        audit: 'audit',
        desktopaudit: 'desktopaudit',
        webaudit: 'webaudit',
        password: 'password',
        passwordsettings: 'password-policy',
        billings: 'bvnreport',
        syncReport: 'sync-report',
        billingsCSV: 'billingreportcsv',
        setting: 'setting',
        report: 'report',
        institution: 'institution',
        template: 'template',
        bulk: 'bulk',
        transactionreport: 'transactionreport'
    }
}
