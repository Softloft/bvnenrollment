import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";

@Injectable()
export class InstitutionService {
    private institution = environment.API_URL.institution;
    private user = environment.API_URL.user;
    private report = environment.API_URL.report;
    private banks = environment.API_URL.banks;
    private paginate = 10;

    constructor(private apiService: ApiHandlerService) {
    }

    getAgentMgrReport(pageNo) {
        return this.apiService.get(`${this.report}/agt-mgr/upl?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getInstitution(pageNo) {
        return this.apiService.get(`${this.institution}?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getAllInstitution(size) {
        return this.apiService.get(`${this.institution}/list`);
    }

    searchInstitution(searchtext, status, pageNo) {
        return this.apiService.get(`${this.institution}?param=${searchtext}&status=${status}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    approve(body, path) {
        return this.apiService.put(`${path}/approval/approve`, body)
    }

    disapprove(body, path) {
        return this.apiService.put(`${path}/approval/disapprove`, body)
    }

    getActivatedAgtMgr() {
        return this.apiService.get(`${this.institution}/activated`);
    }



    insertForm(data, action) {
        if (action === 'new') {
            return this.apiService.post(`${this.institution}`, data);
        } else if (action === 'update') {
            return this.apiService.put(`${this.institution}`, data);
        } else if (action === 'toggle') {
            return this.apiService.put(`${this.institution}/toggle`, data);
        }
    }



    // search
    public getSearchLog(data, pageNo) {
        return this.extractDateProcess(data, pageNo);
    }


    public extractDateProcess(data, pageNo) {
        let fromDate = '';
        let toDate = '';
        let param = '';
        if (data['param']) {
            param = data['param'];
        }

        let params = '';
        if (data['from'] && data['from']['year']) {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (data['to'] && data['to']['year']) {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        params = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10&param=${param}`;

        return this.apiService.get(`${this.report}/agt-mgr/upl${params}`);

    }

    getInstitutionList() {
        return this.apiService.get(`${this.institution}?status=true`);
    }
}
