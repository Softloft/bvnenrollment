import { Injectable } from '@angular/core';
import { ApiHandlerService } from "../api-handler.service";
import { environment } from "../../environments/environment";
import { Cache } from "../../utils/cache";
import { defaultHostName } from "../../utils/check-hostname";

@Injectable()
export class DashboardService {
    private template = environment.API_URL.template;
    private dashboard = environment.API_URL.dashboard;
    private user = environment.API_URL.user;
    private institution = environment.API_URL.institution;
    private transactionreport = environment.API_URL.transactionreport;

    constructor(private apiService: ApiHandlerService) {

    }

    getDashboard() {
        return this.apiService.get(`${this.dashboard}`);
    }

    getDashboardtransactionreport() {
        return this.apiService.get(`${this.transactionreport}/dashboard`);
    }

    getBranchByLga(id) {
        return this.apiService.get(`${this.dashboard}/statistics/state?state=${id}`);
    }

    getAllAgent(pageNo) {
        return this.apiService.get(`${this.dashboard}/agents?pageNumber=${pageNo}&pageSize=10`);
    }

    getAllManager(pageNo) {
        return this.apiService.get(`${this.dashboard}/agentmanagers?pageNumber=${pageNo}&pageSize=10`);
    }

    getInstitutionReport(pageNo) {
        return this.apiService.get(`${this.institution}/stats/bystate?pageNumber=${pageNo}&pageSize=10`);
    }

    getAllBranch(pageNo) {
        return this.apiService.get(`${this.dashboard}/branches?pageNumber=${pageNo}&pageSize=10`);
    }

    getAllEnroller(pageNo) {
        return this.apiService.get(`${this.dashboard}/enrollers?pageNumber=${pageNo}&pageSize=10`);
    }

    getAllEnrollment(pageNo) {
        return this.apiService.get(`${this.dashboard}/enrollments?pageNumber=${pageNo}&pageSize=10&param=`);
    }

    getAllTransactionReport(pageNo) {
        return this.apiService.get(`${this.transactionreport}?pageNumber=${pageNo}&pageSize=10`);
    }

    getStateAndLgaReport(pageNo) {
        return this.apiService.get(`${this.transactionreport}/bystateandlga?pageNumber=${pageNo}&pageSize=10`);
    }

    getStateAgentReport(pageNo, type) {
        return this.apiService.get(`${this.user}/agent/stat/${type}?pageNumber=${pageNo}&pageSize=10`);
    }

    // search
    public getSearchLog(data, pageNo, searchType) {
        return this.extractDateProcess(data, pageNo, searchType);
    }


    public extractDateProcess(data, pageNo, searchType) {
        let fromDate = '';
        let toDate = '';
        let param = '';
        const institutionCode = data['institutionCode'];
        const status = data['status'];
        if (data['param']) {
            param = data['param'];
        }

        let params = '';
        if (data['from'] && data['from']['year']) {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (data['to'] && data['to']['year']) {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        params = `?from=${fromDate}&to=${toDate}&institutionCode=${institutionCode}&validationStatus=${status}&pageNumber=${+pageNo}&pageSize=10&param=${param}`;

        return this.apiService.get(`${this.dashboard}/${searchType}${params}`);
    }

    public searchReport(data, pageNo, type?) {
        let fromDate = '';
        let toDate = '';
        let param = '';
        let institutioncode = '';
        let serviceProvided = '';
        if (data['param']) {
            param = data['param'];
        }
        if (data['serviceProvided']) {
            serviceProvided = data['serviceProvided'];
        }

        if (data['institutioncode']) {
            institutioncode = data['institutioncode'];
        }

        let params = '';
        if (data['from'] && data['from']['year']) {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (data['to'] && data['to']['year']) {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }

        if (type === 'bylga' || type === 'bystate' || type === 'byinstitution') {
            params = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10&param=${param}`;
            return this.apiService.get(`${this.user}/agent/stat/${type}${params}`);
        }
        if (type === 'bystateandlga') {
            params = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10&param=${param}`;
            return this.apiService.get(`${this.transactionreport}/bystateandlga${params}`);
        }
        if (type === 'institution') {
            params = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10&param=${param}`;
            return this.apiService.get(`${this.institution}/stats/bystate${params}`);
        }
        params = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10&param=${param}&agentcode=${institutioncode}&serviceProvided=${serviceProvided}`;
        return this.apiService.get(`${this.transactionreport}${params}`);
    }


    exportdownload(data, downloadtype, searchtype) {
        let fromDate = '';
        let toDate = '';
        let param = '';
        if (data['to']['year'] || data['to'] !== '') {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        if (data['from']['year'] || data['from'] !== '') {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        const institutionCode = data['institutionCode'];
        const validationStatus = data['validationStatus'];
        param = `/download?from=${fromDate}&to=${toDate}&institutionCode=${institutionCode}&validationStatus=${validationStatus}&downloadType=${downloadtype}&vim=${Cache.get('token')}`;

        if (toDate !== '' && fromDate !== '') {
            window.open(`${defaultHostName()}${this.dashboard}/${searchtype}${param}`, "_blank");
        }
    }

    exportReport(data, type?, status?) {
        let fromDate = '';
        let toDate = '';
        let param = '';
        let institutioncode = '';
        let serviceProvided = '';
        if (data['param']) {
            param = data['param'];
        }
        if (data['serviceProvided']) {
            serviceProvided = data['serviceProvided'];
        }

        if (data['institutioncode']) {
            institutioncode = data['institutioncode'];
        }
        if (data['to']['year'] || data['to'] !== '') {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        if (data['from']['year'] || data['from'] !== '') {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (type === 'bylga' || type === 'bystate' || type === 'byinstitution') {
            param = `param=${param}&from=${fromDate}&to=${toDate}&vim=${Cache.get('token')}`;
            return window.open(`${defaultHostName()}bulk/agent/stat/${type}?${param}`, "_blank");
        }
        if (type === 'bystateandlga') {
            param = `param=${param}&from=${fromDate}&to=${toDate}&vim=${Cache.get('token')}`;
            return window.open(`${defaultHostName()}transactionreport/bystateandlga/bulk?${param}`, "_blank");
        }
        if (type === 'sync-report') {
            param = `active=${status}&param=${param}&from=${fromDate}&to=${toDate}&vim=${Cache.get('token')}`;
            return window.open(`${defaultHostName()}bulk/sync-report?${param}`, "_blank");
        }
        if (type === 'institution') {
            param = `param=${param}&from=${fromDate}&to=${toDate}&vim=${Cache.get('token')}`;
            return window.open(`${defaultHostName()}bulk/institution/stats/bystate?${param}`, "_blank");
        }
        param = `/bulk?from=${fromDate}&to=${toDate}&institutionCode=${institutioncode}&serviceProvided=${serviceProvided}&vim=${Cache.get('token')}`;

        if (toDate !== '' && fromDate !== '') {
            window.open(`${defaultHostName()}${this.transactionreport}${param}`, "_blank");
        }
    }

    createDeptBulk(file) {
        return this.apiService.postBulkFile(file, `${this.transactionreport}/template/upload`);
    }

    getBulkFile() {
        return this.apiService.getFile(`${this.transactionreport}/template/upload`);
    }

}
