import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";

@Injectable()
export class AuthService {
    private login = environment.API_URL.login;
    private role = environment.API_URL.role;
    private user = environment.API_URL.user;
    constructor(private apiService: ApiHandlerService) { }

    postLogin(data) {
        return this.apiService.post(this.login, data);
    }

    getTask(id) {
        return this.apiService.get(`${this.role}/${id}/task`);
    }

    logout() {
        console.log('logout service');
        return this.apiService.get(`${this.user}/logout`);
    }
}
