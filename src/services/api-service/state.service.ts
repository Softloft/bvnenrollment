import { Injectable } from '@angular/core';
import { ApiHandlerService } from "../api-handler.service";
import { environment } from "../../environments/environment";

@Injectable()
export class StateService {
    private state = environment.API_URL.state;
    private lga = environment.API_URL.lga;

    constructor(private apiService: ApiHandlerService) {
    }

    getState() {
        return this.apiService.get(`${this.state}/upl`);
    }

    getLga(id) {
        return this.apiService.get(`${this.state}/${id}/lga/upl`);
    }

    getCity(id) {
        return this.apiService.get(`${this.lga}/${id}/city`);
    }
}
