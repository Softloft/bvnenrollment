import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";

@Injectable()
export class EnrollerService {
    private agent = environment.API_URL.agent;

    constructor(private apiService: ApiHandlerService) {
    }

    getAllEnroller(id, pageNo) {
        return this.apiService.get(`${this.agent}/${id}/enroller/upl?pageNumber=${pageNo}&pageSize=10`);
    }

    getbranchesFull() {
        return this.apiService.get(`${this.agent}/list?pageNumber=0&pageSize=100`);
    }

    insertForm(id, data, action) {
        if (action === 'new') {
            console.log('new')
            return this.apiService.post(`${this.agent}/${id}/enroller/upl`, data);
        } else if (action === 'update') {
            return this.apiService.put(`${this.agent}/${id}/enroller/upl`, data);
        } else if (action === 'toggle') {
            return this.apiService.put(`${this.agent}/${id}/enroller/toggle`, data);
        }
    }

}
