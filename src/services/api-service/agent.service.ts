import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";
import { Cache } from "../../utils/cache";

@Injectable()
export class AgentService {
    private bulk = environment.API_URL.bulk;
    private template = environment.API_URL.template;
    private agt_mgr = environment.API_URL.agt_mgr;
    private user = environment.API_URL.user;
    private report = environment.API_URL.report;
    private banks = environment.API_URL.banks;
    private paginate = 10;

    constructor(private apiService: ApiHandlerService) {
    }

    getAgentMgrReport(pageNo) {
        return this.apiService.get(`${this.report}/agt-mgr/upl?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getAgtMgr(pageNo) {
        return this.apiService.get(`${this.agt_mgr}?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getBanks() {
        return this.apiService.get(`${this.banks}?`);
    }

    getActivatedAgtMgr() {
        return this.apiService.get(`${this.agt_mgr}/activated`);
    }

    getAgent(id, pageNo) {
        return this.apiService.get(`${this.agt_mgr}/${id}/agent/upl?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getAllAgent(pageNo) {
        return this.apiService.get(`${this.user}/agent?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    insertForm(data, action) {
        if (action === 'new') {
            return this.apiService.post(`${this.agt_mgr}`, data);
        } else if (action === 'update') {
            return this.apiService.put(`${this.agt_mgr}`, data);
        } else if (action === 'toggle') {
            return this.apiService.put(`${this.agt_mgr}/toggle`, data);
        }
    }

    insertAgentForm(id, data, action) {
        if (action === 'new') {
            return this.apiService.post(`${this.agt_mgr}/${id}/agent/upl`, data);
        } else if (action === 'update') {
            return this.apiService.put(`${this.agt_mgr}/${id}/agent/upl`, data);
        } else if (action === 'toggle') {
            return this.apiService.put(`${this.agt_mgr}/${id}/agent/toggle`, data);
        }
    }


    // search
    public getSearchLog(data, pageNo) {
        return this.extractDateProcess(data, pageNo);
    }


    public extractDateProcess(data, pageNo) {
        let fromDate = '';
        let toDate = '';
        let param = '';
        if (data['param']) {
            param = data['param'];
        }

        let params = '';
        if (data['from'] && data['from']['year']) {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (data['to'] && data['to']['year']) {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        params = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10&param=${param}`;

        return this.apiService.get(`${this.report}/agt-mgr/upl${params}`);

    }

    getAgentbyInstitutionId(id: number) {
        return this.apiService.get(`${this.agt_mgr}/byinstitution/${id}`);
    }

    switch(data) {
        return this.apiService.put(`${this.user}/agent/switch`, data);
    }

    switchManager(data) {
        return this.apiService.put(`${this.user}/agt-mgr/switch`, data);
    }

    resetPassword(data) {
        return this.apiService.put(`${this.user}/agent/password/reset`, data);
    }

    getBulkFile() {
        return this.apiService.getFile(`${this.template}/agent`);
    }

    createDeptBulk(file) {
        return this.apiService.postBulkFile(file, `${this.bulk}/agent`);
    }
}
