import { Injectable } from '@angular/core';
import { ApiHandlerService } from "../api-handler.service";
import { environment } from "../../environments/environment";

@Injectable()
export class AuditService {
    private audit = environment.API_URL.audit;
    private desktopAudit = environment.API_URL.desktopaudit;
    private webAudit = environment.API_URL.webaudit;

    constructor(private apiService: ApiHandlerService) {
    }

    getAllAuditbyId(id, pageNo) {
        return this.apiService.get(`${this.audit}/${id}/audit/upl?pageNumber=${pageNo}&pageSize=10`);
    }

    getDesktopAudit(pageNo) {
        return this.apiService.get(`${this.desktopAudit}?pageNumber=${pageNo}&pageSize=10`);
    }

    getWebAudit(pageNo) {
        return this.apiService.get(`${this.webAudit}?pageNumber=${pageNo}&pageSize=10`);
    }

    getWebAuditById(id) {
        return this.apiService.get(`${this.webAudit}/detail/${id}`);
    }

    public getSearchLog(data, pageNo) {
        return this.extractDateProcess(data, pageNo, this.desktopAudit);
    }

    public extractDateProcess(data, pageNo, url) {
        let fromDate = '';
        let toDate = '';
        let param5 = '';
        let param4 = '';
        let param3 = '';
        let param2 = '';
        let param1 = '';
        let param0 = '';
        const params = data['params'];
        const dataSize = data['dataSize'];
        const className = data['className'];
        if (data['from']['year']) {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (data['to']['year']) {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        param3 = `?from=${fromDate}&to=${toDate}&param=${params}&pageNumber=${+pageNo}&pageSize=10`;
        param2 = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10`;
        param1 = `?from=${fromDate}&pageNumber=${+pageNo}&pageSize=10`;
        param0 = `?username=${params}&pageNumber=${+pageNo}&pageSize=10`;
        param4 = `?dataSize=${params}&pageNumber=${+pageNo}&pageSize=10`;
        param5 = `?className=${params}&pageNumber=${+pageNo}&pageSize=10`;

        if (toDate !== '' && fromDate !== '' && params !== '') {
            return this.apiService.get(`${url}${param3}`);
        } else if (toDate !== '' && fromDate !== '' && params === '') {
            return this.apiService.get(`${url}${param2}`);
        } else if (fromDate !== '' && toDate === '' && params === '') {
            return this.apiService.get(`${url}${param1}`);
        } else if (params !== '' && toDate === '' && fromDate === '') {
            return this.apiService.get(`${url}${param0}`);
        } else if (dataSize !== '') {
            return this.apiService.get(`${url}${param4}`);
        } else if (className !== '') {
            return this.apiService.get(`${url}${param5}`);
        }
    }

    public getSearchLogWeb(data, pageNo) {
        return this.extractDateProcessWeb(data, pageNo, this.webAudit);
    }

    public extractDateProcessWeb(data, pageNo, url) {
        let fromDate = '';
        let toDate = '';
        let param5 = '';
        let param4 = '';
        let param3 = '';
        let param2 = '';
        let param1 = '';
        let param0 = '';
        const params = data['params'];
        const dataSize = data['dataSize'];
        const className = data['className'];
        if (data['from']['year']) {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (data['to']['year']) {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        param3 = `?from=${fromDate}&to=${toDate}&param=${params}&pageNumber=${+pageNo}&pageSize=10`;
        param2 = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10`;
        param1 = `?from=${fromDate}&pageNumber=${+pageNo}&pageSize=10`;
        param0 = `?username=${params}&pageNumber=${+pageNo}&pageSize=10`;
        param4 = `?dataSize=${dataSize}&pageNumber=${+pageNo}&pageSize=10`;
        param5 = `?className=${className}&pageNumber=${+pageNo}&pageSize=10`;

        if (toDate !== '' && fromDate !== '' && params !== '') {
            return this.apiService.get(`${url}${param3}`);
        } else if (toDate !== '' && fromDate !== '' && params === '') {
            return this.apiService.get(`${url}${param2}`);
        } else if (fromDate !== '' && toDate === '' && params === '') {
            return this.apiService.get(`${url}${param1}`);
        } else if (params !== '' && toDate === '' && fromDate === '') {
            return this.apiService.get(`${url}${param0}`);
        } else if (dataSize !== '') {
            return this.apiService.get(`${url}${param4}`);
        } else if (className !== '') {
            return this.apiService.get(`${url}${param5}`);
        }
    }



}
