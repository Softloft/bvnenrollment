import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";

@Injectable()
export class NIBSSService {


    private nibss = `${environment.API_URL.user}/nibss`;

    constructor(private apiService: ApiHandlerService) { }


    // insertForm(data, action) {
    //   if (action === 'new') {
    //     return this.apiService.post(`${this.nibss}`, data);
    //   } else if (action === 'update') {
    //     return this.apiService.put(`${this.nibss}`, data);
    //   } else if (action === 'toggle') {
    //     return this.apiService.put(`${this.nibss}/toggle`, data);
    //   }
    // }

    getNIBSSUserById(id) {
        return this.apiService.get(`${this.nibss}`);
    }

    createNIBSSUser(data) {
        return this.apiService.post(`${this.nibss}`, data);
    }

    getAllNIBSSUsers(pageNo) {
        return this.apiService.get(`${this.nibss}?pageNumber=${pageNo}&pageSize=10`);
    }


    getAllStatus(pageNo, status?, path?) {
        return this.apiService.get(`${path}approval/${status}?pageNumber=${pageNo}&pageSize=10`);

    }


    insertForm(data, action) {
        if (action === 'new') {
            return this.apiService.post(`${this.nibss}`, data);
        } else if (action === 'update') {
            return this.apiService.put(`${this.nibss}`, data);
        } else if (action === 'toggle') {
            return this.apiService.put(`${this.nibss}/toggle`, data);
        }
    }
}
