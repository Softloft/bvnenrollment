import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";

@Injectable()
export class ConfigService {
    private role = environment.API_URL.role;
    private task = environment.API_URL.task;
    private paginate = 10;

    constructor(private apiService: ApiHandlerService) {
    }

    getRole(pageNo) {
        return this.apiService.get(`${this.role}/upl?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getActivatedRole(status) {
        return this.apiService.get(`${this.role}/activated/${status}`);
    }

    insertForm(data, action) {
        if (action === 'new') {
            return this.apiService.post(`${this.role}`, data);
        } else if (action === 'update') {
            return this.apiService.put(`${this.role}`, data);
        } else if (action === 'toggle') {
            return this.apiService.put(`${this.role}/toggle`, data);
        }
    }

    getTasks(id) {
        return this.apiService.get(`${this.role}/${id}/task`);
    }

    getTask() {
        return this.apiService.get(`${this.task}`);
    }

    postTasks(data) {
        console.log('data id :: ', data);
        return this.apiService.put(`${this.role}/task`, data);
    }
}
