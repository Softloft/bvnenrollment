import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";
import { Cache } from "../../utils/cache";
import { defaultHostName } from "../../utils/check-hostname";

@Injectable()
export class BillingsService {
    private role = '';
    private billings = environment.API_URL.billings;
    private default = defaultHostName();
    private billingsCSV = environment.API_URL.billingsCSV;


    constructor(private apiService: ApiHandlerService) {
        this.role = Cache.get('credentials')['user']['userType'];
    }

    getAllBillingsAgentById(id, pageNo) {
        return this.apiService.get(`${this.billings}/${id}`); //According to How to be viewed. Needs to be modified!
    }

    getAllBillings(pageNo, from?, to?, institutionCode?) {
        let url = '';
        if (this.role === 'AGENT') {
            url = `${this.billings}/agent`
        } else if (this.role === 'AGENT_MANAGER') {
            url = `${this.billings}/agent-manager`
        } else if (this.role === 'NIBSS') {
            url = `${this.billings}`
        }

        return this.apiService.get(`${url}?from=${from}&to=${to}&institutionCode=${institutionCode}&pageNumber=${pageNo}&pageSize=10&status=active`);
    }


    public getReportByAgentMgr(data, pageNo) {
        console.log('data :: ', data);
        return this.apiService.get(`${this.billings}?agentManagerCode=${data.superAgentCode}&pageNumber=${pageNo}&pageSize=10`)
    }
    // search
    public getSearchLog(data, pageNo) {
        return this.extractDateProcess(data, pageNo, this.billings);
    }


    public extractDateProcess(data, pageNo, url) {
        console.log('data :: ', data)
        let fromDate = '';
        let toDate = '';
        let param7 = '';
        let param6 = '';
        let param5 = '';
        let param4 = '';
        let param3 = '';
        let param2 = '';
        let param1 = '';
        let param0 = '';
        const agentManagerCode = data['agentManagerCode'];
        const superAgentCode = data['superAgentCode'];
        const status = data['status'];
        console.log('status :: ', status)
        if (data['from'] && data['from']['year']) {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (data['to'] && data['to']['year']) {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        param3 = `?from=${fromDate}&to=${toDate}&agentManagerCode=${agentManagerCode}&pageNumber=${+pageNo}&pageSize=10`;
        param2 = `?from=${fromDate}&to=${toDate}&pageNumber=${+pageNo}&pageSize=10`;
        param6 = `?agentManagerCode=${superAgentCode}&pageNumber=${+pageNo}&pageSize=10`;
        param7 = `?agentManagerCode=${superAgentCode}&status=${status}&pageNumber=${+pageNo}&pageSize=10`;

        if (toDate !== '' && fromDate !== '' && agentManagerCode !== '') {
            return this.apiService.get(`${url}${param3}`);
        } else if (toDate !== '' && fromDate !== '') {
            return this.apiService.get(`${url}${param2}`);
        } else if (superAgentCode !== '' && status === '') {
            return this.apiService.get(`${url}${param6}`);
        } else if (superAgentCode !== '' && status !== '') {
            return this.apiService.get(`${url}${param7}`);
        }
    }

    /**
     * Export billings to csv and download.
     */
    exportToCSV(from, to, acceptType?: string, institutionCode?) {
        window.open(`${this.default}${this.billingsCSV}?from=${from}&to=${to}&institutionCode=${institutionCode}&vim=${Cache.get('token')}`, "_blank");
        // return this.apiService.get(`${this.default}${this.billingsCSV}?from=${from}&to=${to}`);
    }


    onGetCount(agtCode, status, pageNo) {
        return this.apiService.get(`${this.billings}?pageNumber=${pageNo}&pageSize=10&agentCode=${agtCode}&status=${status}`);
    }
}
