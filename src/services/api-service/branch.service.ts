import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";

@Injectable()
export class BranchService {
    private agent = environment.API_URL.agent;

    constructor(private apiService: ApiHandlerService) {
    }

    getAllBranches(id, pageNo) {
        return this.apiService.get(`${this.agent}/${id}/branch/upl?pageNumber=${pageNo}&pageSize=10`);
    }

    getActivatedBranch(id) {
        return this.apiService.get(`${this.agent}/${id}/branch/activated`);
    }

    getbranchesFull() {
        return this.apiService.get(`${this.agent}/list?pageNumber=0&pageSize=100`);
    }

    insertForm(id, data, action) {
        if (action === 'new') {
            return this.apiService.post(`${this.agent}/${id}/branch/upl`, data);
        } else if (action === 'update') {
            return this.apiService.put(`${this.agent}/${id}/branch/upl`, data);
        } else if (action === 'toggle') {
            return this.apiService.put(`${this.agent}/${id}/branch/toggle`, data);
        }
    }

}
