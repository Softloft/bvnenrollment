import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { Router } from "@angular/router";
import { Cache } from '../utils/cache';


@Injectable()
export class PushToDashboardService implements OnInit, OnDestroy {
    private pusherTimer: any;
    userData: any;
    constructor(private userService: UserService, private router: Router) {
        if (Cache.get('credentials') || Cache.get('bvn-auth-token')) {
            this.userData = Cache.get('credentials')
        } else {
            this.userService.logout();
        }
    }
    ngOnInit() {
        this.pushToDashboard();
    }
    private timer() {
        this.pusherTimer = setTimeout(() => {
            this.pushToDashboard();
        }, 5000);
    }

    private routeMe() {

        clearTimeout(this.pusherTimer);
        // console.log('DETAILS::: ', this.userData);
        (this.userData['user']['change_password']) ? this.router.navigate(['/profile']) : this.router.navigate(['/dashboard']);
        /*    switch (this.userService.getAuthUser()['user_type']) {
                case 'App\\Merchant':
                    if (!this.userService.getAuthUser()['boarding_status']) { // check if user onboarding status to route to onboarding page
                        window.location.href = window.location.origin + '/dashboard/merchants/onboarding';
                    } else {
                        window.location.href = window.location.orPushToDashboardServiceigin + '/dashboard';
                    }
                    break;
                case 'App\\MerchantUser':
                    if (!this.userService.getAuthUser()['boarding_status'] && this.userService.getAuthUser()['user']['role'] !== 'user') {
                        // check if user onboarding status to route to onboarding page
                        window.location.href = window.location.origin + '/dashboard/merchants/onboarding';
                        return null;
                    } else {
                        window.location.href = window.location.origin + '/dashboard';
                    }
                    break;
                default:
                    window.location.href = window.location.origin + '/dashboard';
                    break;
            }*/
    }

    pushToDashboard() {
        if (this.userService.isLoggedIn()) {
            this.routeMe();
        } else {
            // this.timer();
        }
    }
    ngOnDestroy() {
        clearTimeout(this.pusherTimer);
    }
}
