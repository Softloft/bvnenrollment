import { Injectable, OnDestroy } from '@angular/core';
import { ApiHandlerService } from "./api-handler.service";
import { environment } from "../environments/environment";

@Injectable()
export class PasswordService implements OnDestroy {
    strength: number;
    private password = environment.API_URL.password;
    private passwordsettings = environment.API_URL.passwordsettings;
    private setting = environment.API_URL.setting;

    constructor(private apiService: ApiHandlerService) {
    }

    testPassword(pwString, minLenght) {
        this.strength = 0;
        this.strength += pwString.length >= minLenght ? 1 : 0;
        this.strength += /[A-Z]/.test(pwString) ? 1 : 0;
        this.strength += /[a-z]+/.test(pwString) ? 1 : 0;
        this.strength += /[0-9]+/.test(pwString) ? 1 : 0;
        this.strength += /[\W]+/.test(pwString) ? 1 : 0;
        this.strength += /[\d]+/.test(pwString) ? 1 : 0;
        this.strength += /[\s]+/.test(pwString) ? 1 : 0;
        this.strength += /!@#\$%\^&*\)-:;\.,\(\|+\^/.test(pwString) ? 1 : 0;
        // console.log('strength: ', this.strength);
    }


    public changePassword(data) {
        return this.apiService.put(`${this.password}/update-password`, data);
    }

    public passwordPolicy() {
        return this.apiService.get(`${this.password}/passwordpolicy`);
    }

    public sendResetLink(data) {
        return this.apiService.post(`${this.password}/reset`, data);
    }

    public validateRecoveryCode(code: object) {
        return this.apiService.post(`${this.password}/validate-recovery-code`, code);
    }

    public resetPassword(password) {
        return this.apiService.put(`${this.password}/update`, password);
    }

    ngOnDestroy() {
        this.strength = 0;
    }

    // PASSWORD SETTINGS

    public getPasswordSettings() {
        return this.apiService.get(`${this.passwordsettings}/settings`)
    }

    public desktopSetting() {
        return this.apiService.get(`${this.setting}/desktop`)
    }

    public updatePassword(data) {
        return this.apiService.put(`${this.passwordsettings}/update`, data)
    }

    public validatePassword(data) {
        return this.apiService.post(`${this.passwordsettings}/validate`, data)
    }

    saveAmount(data) {
        return this.apiService.put(`${this.setting}/amount?amount=${data['amount']}`)
    }

    saveVersion(data) {
        return this.apiService.put(`${this.setting}/enrollment-versions?supportedVersions=${data['version']}`)
    }

    saveSource(data) {
        return this.apiService.put(`${this.setting}/enrollment-source?supportedSources=${data['source']}`)
    }

    saveDesktopSetting(data, status, id) {
        if (status) {
            const body = { name: data.value.name, value: data.value.value, id };
            return this.apiService.put(`${this.setting}/desktop`, body);
        } else {
            return this.apiService.post(`${this.setting}/desktop`, data.value);
        }

    }

    getAmount() {
        return this.apiService.get(`${this.setting}/amount`)
    }

    getVersion() {
        return this.apiService.get(`${this.setting}/enrollment-versions`)
    }

    getSource() {
        return this.apiService.get(`${this.setting}/enrollment-source`)
    }
}
