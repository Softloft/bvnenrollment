import { Injectable } from '@angular/core';
import { ApiHandlerService } from "./api-handler.service";
import { environment } from "../environments/environment";

@Injectable()
export class MemberSyncService {

    private syncReport = environment.API_URL.syncReport;
    constructor(private apiService: ApiHandlerService) { }

    getSyncReport(from?: string, to?: string) {
        if (!from || !to) {
            return this.apiService.get(`${this.syncReport}`);

        }
        return this.apiService.get(`${this.syncReport}?from=${from}&to=${to}`);
    }

    getSyncReportDashboard() {
        return this.apiService.get(`${this.syncReport}/dashboard`);
    }

    getSyncReportByAgent(agentId: string, from?: string, to?: string) {
        if (!from || !to) {
            return this.apiService.get(`${this.syncReport}/${agentId}`);

        }
        return this.apiService.get(`${this.syncReport}/${agentId}?from=${from}&to=${to}`);
    }

    getActiveSync(pageNo, activeStatus, searchParam?) {
        let username = '';
        let from = '';
        let to = '';
        if (searchParam['to']) {
            to = `${searchParam['to']['year']}-${searchParam['to']['month']}-${searchParam['to']['day']}`;
        }
        if (searchParam['from']) {
            from = `${searchParam['from']['year']}-${searchParam['from']['month']}-${searchParam['from']['day']}`;
        }
        if (searchParam['username']) {
            username = searchParam['username'];
        }
        return this.apiService.get(`${this.syncReport}?active=${activeStatus}&params=${username}&from=${from}&to=${to}&pageNumber=${pageNo}&pageSize=10`);
    }


    getActiveAgentMgrSync(pageNo, activeStatus, code, searchParam?) {
        let username = '';
        let from = '';
        let to = '';
        console.log('search params :: ', searchParam)
        if (searchParam['to']) {
            to = `${searchParam['to']['year']}-${searchParam['to']['month']}-${searchParam['to']['day']}`;
        }
        if (searchParam['from']) {
            from = `${searchParam['from']['year']}-${searchParam['from']['month']}-${searchParam['from']['day']}`;
        }
        if (searchParam['username']) {
            username = searchParam['username'];
        }
        return this.apiService.get(`${this.syncReport}/${code}?active=${activeStatus}&params=${username}&from=${from}&to=${to}&pageNumber=${pageNo}&pageSize=10`);
    }

    getActiveAgentMgr(pageNo, activeStatus, searchParam, code) {

        let username = '';
        let from = '';
        let to = '';
        console.log('search params :: ', searchParam)
        if (searchParam['to']) {
            to = `${searchParam['to']['year']}-${searchParam['to']['month']}-${searchParam['to']['day']}`;
        }
        if (searchParam['from']) {
            from = `${searchParam['from']['year']}-${searchParam['from']['month']}-${searchParam['from']['day']}`;
        }
        if (searchParam['username']) {
            username = searchParam['username'];
        }
        return this.apiService.get(`${this.syncReport}/${code}?active=${activeStatus}&params=${username}&from=${from}&to=${to}&pageNumber=${pageNo}&pageSize=10`);
    }
}
