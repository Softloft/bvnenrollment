import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/observable/from";
import "rxjs/add/operator/debounce";
import "rxjs/add/operator/delay";
import {
    HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
    HttpResponse
} from "@angular/common/http";
import { environment } from "../environments/environment";
import { CopingsService } from "./copings.service";
import { defaultHostName } from "../utils/check-hostname";

@Injectable()
export class PaginatorInterceptorService implements HttpInterceptor {



    constructor(private copings: CopingsService) {
    }

    private constructPaging(response) {
        response['body']['number'] += 1;
        const url = defaultHostName + environment.API_URL.role;
        const nextPage = ((response['body']['number'] + 1) > response['body']['totalPages']) ? null : response['body']['number'] + 1;
        const previousPage = ((response['body']['number'] - 1) < 1) ? null : response['body']['number'] - 1;
        console.log('nextPage=', nextPage, 'previousPage=', previousPage);
        const lastPage = Math.ceil(response['body']['totalPages'] / response['body']['size']);
        response['body']["total"] = response['body']['totalElements'];
        response['body']["per_page"] = response['body']['size'];
        response['body']["current_page"] = response['body']['number'];
        response['body']["next_page_url"] = `${url}?pageNumber=` + nextPage;
        if (response['body']["current_page"] === 1) {
            response['body']["prev_page_url"] = null;
        } else {
            response['body']["prev_page_url"] = `${url}?pageNumber=` + previousPage;
        }

        response['body']["path"] = url;
        response['body']["from"] = 1;
        response['body']["last_page"] = response['body']['totalPages'];
        response['body']["to"] = lastPage;
        response['body']["data"] = response['body']['content'];
        response['body']["content"] = null;
        console.log('responseResponse=', response['body']);
        this.reCreateBody(response);
    }

    private reCreateBody(response) {
        const body = this.copings.deepCopy(response.body);
        response.body = {
            data: body
        };
    }

    private updaterequest(req) {
        const reqW = JSON.parse(JSON.stringify(req));
        console.log('urlA==', reqW.url);
        const pos = req.url.indexOf("pageNumber");
        if (pos > -1) {
            const eqPos = req.url.indexOf("=");
            const eqAnd = req.url.indexOf('&');
            let pageNumber = Number(req.url.substring(eqPos + 1, eqAnd));
            pageNumber = (pageNumber === 0) ? pageNumber : pageNumber - 1;
            const url = req.url.substring(0, eqPos + 1) + pageNumber + req.url.substring(eqAnd);
            console.log('a=', req.url.substring(0, eqPos));
            console.log('b=', req.url.substring(eqAnd));
            console.log('Value=', pageNumber, 'url', url);

            req.url = url;
            req.urlWithParams = url;
        }
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('request=', request);

        this.updaterequest(request);

        return next.handle(request).delay(300).do((response: HttpEvent<any>) => {

            if (response instanceof HttpResponse) {
                console.log('dataEv4nt=', response['body']);
                // do stuff with response if you want
                // condition is needed to know if it is a paging url or not. u can get url with response['url'] or other param from data.
                // such as:
                if (request.url.indexOf('pageNumber') > -1 && response['body'] && response['body']['content']) {
                    console.log('Start Formatting');
                    this.constructPaging(response);
                } else {
                    this.reCreateBody(response);
                }
            }
        }, (err: any) => {

            Observable.throw(err || 'Server Error');
            /*if (err instanceof HttpErrorResponse) {
             Observable.throw(err || 'Server Error');
             }*/
        });


    }
}