import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScriptLoaderService } from "./_services/script-loader.service";
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";
import { ConfigService } from "../services/api-service/config.service";
import { ApiHandlerService } from "../services/api-handler.service";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { UserService } from "../services/user.service";
import { NotificationService } from "../services/notification.service";
import { ValidationErrorService } from "../services/validation-error.service";
import { EventsService } from "../services/event.service";
import { NotificationModule } from "../shared/modules/notification.module";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LarangPaginatorModule } from "larang-paginator";
import { NIBSSService } from "../services/api-service/nibss.service";
import { CopingsService } from "../services/copings.service";
import { AgentService } from "../services/api-service/agent.service";
import { StateService } from "../services/api-service/state.service";
import { BranchService } from "../services/api-service/branch.service";
import { EnrollerService } from "../services/api-service/enroller.service";
import { AuthService } from "../services/api-service/auth.service";
import { NgxPaginationModule } from "ngx-pagination";
import { NgPureDatatableModule } from "ng-pure-datatable";
import { PaginatorInterceptorService } from "../services/paginator-interceptor.service";
import { Select2Module } from "ng2-select2";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { PushToDashboardService } from "../services/push-to-dashboard.service";
import { AllEnrollerComponent } from './theme/pages/default/all-enroller/all-enroller.component';
import { AllBranchComponent } from './theme/pages/default/all-branch/all-branch.component';
import { AuditService } from "../services/api-service/audit.service";
import { PasswordService } from "../services/password.service";
import { CheckPasswordGuard } from "./auth/_guards/check-password.guard";
import { RoleGuardService } from "./auth/_guards/role-guard.service";
import { DashboardService } from "../services/api-service/dashboard.service";
import { MemberStatusComponent } from './theme/pages/default/member-status/member-status.component';
import { ListAgentComponent } from './theme/pages/default/list-agent/list-agent.component';
import { SearcherService } from "../services/searcher.service";
import { InstitutionComponent } from './theme/pages/default/institution/institution.component';
import { InstitutionService } from "../services/api-service/institution.service";
import { GuardReportGuard } from "./auth/_guards/guardReport.guard";

@NgModule({
    declarations: [
        ThemeComponent,
        AppComponent],
    imports: [
        LayoutModule,
        AuthModule,
        BrowserModule,
        NgxPaginationModule,
        NgPureDatatableModule.forRoot(),
        BrowserAnimationsModule,
        AppRoutingModule,
        ThemeRoutingModule,
        AuthModule,
        Select2Module,
        NgbModule.forRoot(),
        BootstrapSwitchModule.forRoot(),
        BrowserAnimationsModule,
        NotificationModule,
        HttpClientModule,
        AngularMultiSelectModule,
        LarangPaginatorModule.forRoot(),

    ],
    providers: [
        ScriptLoaderService,
        ConfigService,
        AuditService,
        AgentService,
        ApiHandlerService,
        CheckPasswordGuard,
        GuardReportGuard,
        UserService,
        PasswordService,
        NotificationService,
        ValidationErrorService,
        NIBSSService,
        CopingsService,
        StateService,
        EventsService,
        BranchService,
        AuthService,
        PushToDashboardService,
        EnrollerService,
        RoleGuardService,
        DashboardService,
        SearcherService,
        InstitutionService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}