import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { Router } from "@angular/router";
import { UserService } from "../../../../services/user.service";
import { Cache } from "../../../../utils/cache";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../../../services/api-service/auth.service";
import { NotificationService } from "../../../../services/notification.service";
import { SearcherService } from "../../../../services/searcher.service";

declare let mLayout: any;
declare const $;

@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    public details = [];
    public profile = [];
    public userRole = '';
    public searchOption = 'Agent';
    public searchtext = '';
    public networkStatus: any;
    itemListRole = [];
    createForm: FormGroup;
    public booleans = {
        loadingSearch: false
    };
    public list = {
        data: []
    }


    static formData = function() {
        return {
            bvn: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
            state: ['', Validators.compose([Validators.required])],
            city: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private router: Router,
        private userService: UserService,
        private searchservice: SearcherService,
        private notification: NotificationService,
        private authservice: AuthService,
        private fb: FormBuilder) {
        this.createForm = this.fb.group(HeaderNavComponent.formData())
    }

    ngOnInit() {
        this.profile = Cache.get('credentials');
        console.log('role :: ', this.profile['makerCheckerType']);
        this.details = Cache.get('credentials')['user'];
        const getRole = Cache.get('credentials')['user']['role']['name'];
        if (getRole && getRole !== '') {
            this.userRole = getRole.toLowerCase();
        }
        // console.log('auth :: ', this.details);

        window.setInterval(() => {
            this.networkStatus = window.navigator.onLine;
            if (!navigator.onLine) {
                this.notification.error('Network disconnected, please connect back');
            }
        }, 5000);

    }

    ngAfterViewInit() {

        mLayout.initHeader();

    }

    onLogout() {
        Cache.set('loginStatus', 'yes');
        this.authservice.logout()
            .subscribe((response) => {
                this.userService.logout();
                console.log('logout details :: ', response)
                this.router.navigate(['login']);
            },
            error => {
                console.log('error');
                // this.notification.error('Unable to logout due to network', error);
            })
    }

    onOpen() {
        $('#CreateSettings').modal('show');
    }

    onSearchType(event) {
        this.searchOption = event;
    }

    searchMethod() {
        console.log('searchtext :: ', this.searchtext);
        this.booleans.loadingSearch = true;
        if (this.searchOption === 'Agent') {
            this.searchservice.databaseSearch(this.searchtext, 'agent', 0)
                .subscribe((response) => {
                    this.booleans.loadingSearch = false;
                    if (response.content.length === 0) {
                        this.notification.error('No Result Found for this Agent Code')
                    } else {
                        $('#viewModalLookup').modal('show');
                        this.list.data = response.content[0];
                    }
                },
                error => {
                    this.booleans.loadingSearch = false;
                    this.notification.error('Unable to Search', error);
                })
        } else if (this.searchOption === 'Enroller') {
            this.searchservice.databaseSearch(this.searchtext, 'enroller', 10)
                .subscribe((response) => {
                    this.booleans.loadingSearch = false;
                    if (response.content.length === 0) {
                        this.notification.error('No Result Found for this Enroller Code')
                    } else {
                        this.list.data = response.content[0];
                        $('#viewModalLookup').modal('show');
                    }
                },
                error => {
                    this.booleans.loadingSearch = false;
                    this.notification.error('Unable to Search', error);
                })
        }
        console.log('searchOption :: ', this.searchOption);

    }


}