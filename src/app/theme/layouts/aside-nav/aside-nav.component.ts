import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { Cache } from "../../../../utils/cache";

declare let mLayout: any;
@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {
    public profile;
    public userRole = '';

    constructor() {

    }
    ngOnInit() {
        this.profile = Cache.get('credentials');
        const getRole = Cache.get('credentials')['user']['role']['name'];
        if (getRole && getRole !== '') {
            this.userRole = getRole.toLowerCase();
        }
    }
    ngAfterViewInit() {

        mLayout.initAside();

    }

}