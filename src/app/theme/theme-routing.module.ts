import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "../auth/_guards/auth.guard";
import { AuthModule } from "../auth/auth.module";
import { CheckPasswordGuard } from "../auth/_guards/check-password.guard";
import { RoleGuardService } from "../auth/_guards/role-guard.service";
import { MemberSyncService } from "../../services/member-sync.service";
import { GuardReportGuard } from "../auth/_guards/guardReport.guard";

const routes: Routes = [
    {
        path: "",
        component: ThemeComponent,
        canActivate: [AuthGuard, CheckPasswordGuard],
        canLoad: [AuthGuard],
        children: [
            {
                "path": "",
                redirectTo: "dashboard",
                pathMatch: "full"
            },
            {
                "path": "dashboard",
                "loadChildren": "./pages/default/index/index.module#IndexModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "nibss",
                "loadChildren": "./pages/default/nibss\/nibss.module#NIBSSModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "all-agent",
                "loadChildren": "./pages/default/all-agent/all-agent.module#AllAgentModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "agents",
                "loadChildren": "./pages/default/list-agent/list-agent.module#ListAgentModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "managers",
                "loadChildren": "./pages/default/list-manager/list-manager.module#ListManagerModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "all-enroller",
                "loadChildren": "./pages/default/all-enroller/all-enroller.module#AllEnrollerModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "all-branch",
                "loadChildren": "./pages/default/all-branch/all-branch.module#AllBranchModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "agent",
                "loadChildren": "./pages/default/agent-managers/agent-managers.module#AgentManagersModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "branches",
                "loadChildren": "./pages/default/branch/branch.module#BranchModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "audit",
                "loadChildren": "./pages/default/audit/audit.module#AuditModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "enroller",
                "loadChildren": "./pages/default/enroller/enroller.module#EnrollerModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "enrollment-report",
                "loadChildren": "./pages/default/enrollers-report/enrollers-report.module#EnrollersReportModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "report",
                "loadChildren": "./pages/default/enrollment-report/enrollment-report.module#EnrollmentReportModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "profile",
                "loadChildren": "./pages/default/profile/profile.module#ProfileModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "config",
                "loadChildren": "./pages/default/configuration/configuration.module#ConfigurationModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "settings",
                "loadChildren": "./pages/default/password-setting/password-setting.module#PasswordSettingModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "billing",
                "loadChildren": "./pages/default/billings/billings.module#BillingsModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "sync-report",
                "loadChildren": "./pages/default/member-status/members.module#MembersModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "agent-manager-report",
                "loadChildren": "./pages/default/report/report.module#ReportModule",
                canActivate: [CheckPasswordGuard, GuardReportGuard]
            },
            {
                "path": "sync-agentMgr",
                "loadChildren": "./pages/default/member-status/filter-super-agent/filter-super-agent.module#FilterSuperAgentModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "institution",
                "loadChildren": "./pages/default/institution/institution.module#InstitutionModule",
                canActivate: [CheckPasswordGuard]
            },
            {
                "path": "404",
                "loadChildren": "./pages/default/not-found/not-found.module#NotFoundModule",
                canActivate: [CheckPasswordGuard]
            }
        ]
    },
    {
        "path": "**",
        "redirectTo": "404",
        "pathMatch": "full"
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes), AuthModule],
    providers: [MemberSyncService],
    exports: [RouterModule]
})
export class ThemeRoutingModule { }