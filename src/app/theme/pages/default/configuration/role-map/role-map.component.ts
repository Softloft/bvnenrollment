import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../../../../../services/api-service/config.service';
import { NotificationService } from '../../../../../../services/notification.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from "../../../../../../environments/environment";
import { NgPureDataTableEventService, NgSearchTypesEnum } from "ng-pure-datatable";
import { Cache } from "../../../../../../utils/cache";

declare const $;

@Component({
    selector: 'app-role-map',
    templateUrl: 'role-map.component.html',
    styleUrls: ['./role-map.component.css']
})
export class RoleMapComponent implements OnInit {
    public id = 0;
    public totalElement = 0;
    public p = 1;
    public selectedModule = '';
    public role = '';
    public moduleName = 'Role Mapping';
    public isCollapsed = false;
    public createForm: FormGroup;

    public booleans = {
        showUpdate: false,
        showDrop: false,
        loader: false,
        loadingStatus: false,
        descriptionStatus: false,
        nameStatus: false,
        usertypeStatus: false,
    };
    public list = {
        allContent: [],
        selectedId: [],
        data: [],
        tasks: [],
        moduleTasks: [],
        task: '',
        alltasks: [],
        newTasks: [],
        moduleName: [],
        temporaryTask: [],
    };
    static formdata = function() {
        return {
            description: ['', Validators.compose([])],
            name: ['', Validators.compose([Validators.required])],
            userType: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private configService: ConfigService,
        private notification: NotificationService,
        private ngPureDataTableEventService: NgPureDataTableEventService,
        private fb: FormBuilder,
        private router: Router) {
        this.createForm = this.fb.group(RoleMapComponent.formdata());
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        const getRole = Cache.get('credentials')['user']['role']['name'];
        if (getRole.toLowerCase() === 'report viewer') {
            return;
        }
        if (this.role === 'NIBSS') {
            this.loadFunction()
        }
    }

    loadFunction() {
        this.list.tasks = Cache.get('tasks');
        this.onGetRole(this.p - 1);
        this.getTask();
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        this.onGetRole(this.p - 1);
    }

    onGetRole(pageNo) {
        this.booleans.loader = true;
        this.configService.getRole(pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;

            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    insertForm(action, data?: any) {
        console.log('new :: ', this.booleans.usertypeStatus);
        console.log('this.list.newTasks.length :: ', this.list.newTasks.length);
        if (action === 'update') {
            this.list.data = this.createForm.value;
            if ((this.booleans.nameStatus || this.booleans.descriptionStatus || this.booleans.usertypeStatus) && this.list.newTasks.length > 0) {
                this.postTask();
                this.post(action);
            } else if ((!this.booleans.nameStatus && !this.booleans.descriptionStatus && !this.booleans.usertypeStatus && this.list.newTasks.length > 0)) {
                this.postTask();
            } else if ((this.booleans.nameStatus || this.booleans.descriptionStatus || this.booleans.usertypeStatus) && this.list.newTasks.length === 0) {
                this.post(action);
            }
        } else if (action === 'toggle') {
            this.list.data = data;
            this.post(action);
        } else {
            this.list.data = this.createForm.value;
            this.post(action);
        }

    }

    private post(action) {
        this.booleans.loadingStatus = true;
        this.configService.insertForm(this.list.data, action)
            .subscribe((response) => {
                this.booleans.loadingStatus = false;
                if (action === 'update' || action === 'toggle') {
                    this.list.allContent.forEach((val, i) => {
                        if (val.id === this.list.data['id']) {
                            this.list.allContent[i] = response;
                        }
                    });
                    this.notification.success('Update was successful');
                } else {
                    this.list.allContent.unshift(response);
                    // this.list.allContent = [...this.list.allContent];
                }
                $('#CreateModal').modal('hide');
                this.createForm.reset();
            },
            error => {
                this.booleans.loadingStatus = false;
                this.notification.error(`Unable to ${action} ${this.moduleName}`, error)
            });
    }

    checkChangesOnName(event) {
        this.booleans.nameStatus = true;
    }

    checkChangesOnDescription(event) {
        this.booleans.descriptionStatus = true;
    }

    checkChangesOnUsertype(event) {
        this.booleans.usertypeStatus = true;
    }

    postTask() {
        this.booleans.loadingStatus = true;
        this.configService.postTasks({ id: this.id, taskIds: this.list.selectedId })
            .subscribe((response) => {
                this.notification.success('Update was successful');
                this.booleans.loadingStatus = false;
                //        this.getTask();
                $('#CreateModal').modal('hide');
            },
            error => {
                this.booleans.loadingStatus = false;
                this.notification.error('Unable to update task, please retry', error);
            })
    }

    onOpen() {
        this.booleans.showUpdate = false;
        this.createForm.reset();
        $('#CreateModal').modal('show');
    }

    onEdit(data) {
        this.id = data.id;
        this.booleans.descriptionStatus = false;
        this.booleans.nameStatus = false;
        this.list.newTasks = [];
        this.list.selectedId = [];
        this.list.data = data;
        this.createForm = this.fb.group(data);
        this.booleans.showUpdate = true;
        data.tasks.forEach(val => {
            this.list.selectedId.push(val.id);
        });

        $('#CreateModal').modal('show');
    }

    onAddTask(id) {
        console.log('select task', id);
        const index = this.list.selectedId.indexOf(id);
        if (index > -1) {
            this.list.selectedId.splice(index, 1);
        } else {
            this.list.selectedId.push(id);
            this.list.newTasks.push(id);
        }
    }

    getTask() {
        this.configService.getTask()
            .subscribe((response) => {
                console.log('new tasks :: ', response);
                this.list.alltasks = response;
                this.list.alltasks.forEach((value) => {
                    if (this.list.moduleName.indexOf(value['module']) === -1) {
                        this.list.moduleName.push(value['module']);
                    }
                });

            },
            error => {
                this.notification.error('Unable to load tasks', error);
            })
    }


    /**
     *  on selecting the module name when assigning a task,
     *  the selected module name is used get its tasks
     *  which is populated on the checkbox option
     * selected task on creating group
     * @param moduleName
     */
    selectTask(moduleName) {
        if (this.list.task === moduleName && this.booleans.showDrop) {
            this.booleans.showDrop = false;
        } else {
            this.booleans.showDrop = true;
        }
        this.list.moduleTasks = [];
        this.list.task = moduleName;
        this.list.alltasks.forEach((value) => {
            if (value['module'] === moduleName) {
                this.list.moduleTasks.push(value);
            }
        });
        this.selectedModule = `#collapseOne${moduleName}`
    }


    onView(data) {
        this.list.data = data;
        $('#viewModal_1').modal('show');
    }

    onViewTask() {
        $('#collapseThree').modal('show');
    }

    insert() {
        if (!this.booleans.showUpdate) {
            this.insertForm('new');
        } else if (this.booleans.showUpdate) {
            this.insertForm('update');
        }
    }
}
