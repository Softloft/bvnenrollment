import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationComponent } from './configuration.component';
import { DefaultComponent } from "../default.component";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { RoleMapComponent } from './role-map/role-map.component';
import { ReactiveFormsModule } from "@angular/forms";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxPaginationModule } from "ngx-pagination";
import { NgPureDatatableModule } from "ng-pure-datatable";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ConfigurationComponent,
                "children": [
                    {
                        "path": "",
                        "component": RoleMapComponent
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        NgPureDatatableModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule
    ],
    declarations: [ConfigurationComponent, RoleMapComponent]
})
export class ConfigurationModule { }
