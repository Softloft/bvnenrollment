import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from "../../../../_services/script-loader.service";

declare const $;
@Component({
    selector: 'app-configuration',
    templateUrl: './configuration.component.html',
    styles: []
})
export class ConfigurationComponent implements OnInit, AfterViewInit {
    constructor(private _script: ScriptLoaderService) {

    }
    ngOnInit() {
        this._script.loadScripts('app-configuration',
            ['assets/app/js/dashboard.js']);

    }
    ngAfterViewInit() {


    }

}
