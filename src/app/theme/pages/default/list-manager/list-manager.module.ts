import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListManagerComponent } from './list-manager.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { NgPureDatatableModule } from "ng-pure-datatable";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { DefaultComponent } from "../default.component";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ListManagerComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        NgPureDatatableModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        PipeModule,
        AngularMultiSelectModule,
        FormsModule
    ],
    declarations: [ListManagerComponent]
})
export class ListManagerModule { }
