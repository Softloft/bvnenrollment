import { Component, OnInit } from '@angular/core';
import { DashboardService } from "../../../../../services/api-service/dashboard.service";
import { NotificationService } from "../../../../../services/notification.service";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

declare const $;

@Component({
    selector: 'app-list-manager',
    templateUrl: 'list-manager.component.html',
    styles: []
})
export class ListManagerComponent implements OnInit {
    public p = 1;
    public download = '';
    public totalElement = 0;
    public toDate: any;
    public fromDate: any;
    public toDate1: any;
    public fromDate1: any;
    public search = {
        to: [],
        from: []
    };
    public id = 0;
    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false
    };

    public list = {
        allContent: [],
    };


    constructor(private dashboardservice: DashboardService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadFunction();
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            console.log('search')
            this.getSearchLog(this.p - 1);
        } else {
            console.log('get')
            this.getAllManager(this.p - 1);
        }
    }

    loadFunction() {
        this.getAllManager(this.p - 1);
    }

    onSelectDownType(type) {
        this.download = type;
        console.log('download type :: ', type)
    }

    getSearchLog(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.dashboardservice.getSearchLog(this.search, pageNo, 'agentmanagers')
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                console.log('response from search ', response);
                if (response.content.length === 0) {
                    this.booleans.searchStatus = false;
                    this.notification.error('No result Found');
                } else {
                    // console.log('all :: ', response)
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })
    }

    getAllManager(pageNo) {
        console.log('pageNo :: ', pageNo)
        this.booleans.searchStatus = false;
        this.dashboardservice.getAllManager(pageNo)
            .subscribe((response) => {
                console.log('all manager :: ', response)
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                console.log('errors :: ', error)
                this.notification.error('Unable to load Branch', error);
            })
    }

    onOpen() {
        $('#CreateModal').modal('show');
    }

    exportDownload() {
        this.search['to'] = this.toDate1;
        this.search['from'] = this.fromDate1;
        this.dashboardservice.exportdownload(this.search, this.download, 'agentmanagers')
        /*.subscribe((response) => {
                this.booleans.loadingSearch = false;
                //  window.open(response, "_blank");
                console.log('response from export ', response);
                if(response.content.length === 0){
                    this.booleans.searchStatus= false;
                    this.notification.error('No result Found');
                }else{
                    // console.log('all :: ', response)
                    this.list.allContent = response.content;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })*/
    }
}
