import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllBranchComponent } from "./all-branch.component";
import { RouterModule, Routes } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LayoutModule } from "../../../layouts/layout.module";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DefaultComponent } from "../default.component";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";

const RouterRoute: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            { path: '', component: AllBranchComponent }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(RouterRoute),
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        LayoutModule,
        AngularMultiSelectModule,
        NgbModule
    ],
    declarations: [
        AllBranchComponent
    ]
})
export class AllBranchModule { }
