import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { BranchService } from "../../../../../services/api-service/branch.service";
import { StateService } from "../../../../../services/api-service/state.service";
import { ConfigService } from "../../../../../services/api-service/config.service";
import { NotificationService } from "../../../../../services/notification.service";
import { Route, ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../utils/cache";
import { AgentService } from "../../../../../services/api-service/agent.service";
import { DashboardService } from "../../../../../services/api-service/dashboard.service";


declare const $;

@Component({
    selector: 'app-all-branch',
    templateUrl: 'all-branch.component.html',
    styles: []
})
export class AllBranchComponent implements OnInit {
    public p = 1;
    public totalElement = 0;
    public id = 0;
    public download: any;
    public toDate: any;
    public fromDate: any;
    public toDate1: any;
    public fromDate1: any;
    public search = {
        to: [],
        from: []
    };
    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false,

    };

    public list = {
        allContent: [],
    };


    constructor(private branchService: BranchService,
        private dashboardservice: DashboardService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadFunction();
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            console.log('search')
            this.getSearchLog(this.p - 1);
        } else {
            console.log('get')
            this.getAllBranch(this.p - 1);
        }
    }


    loadFunction() {
        this.getAllBranch(this.p - 1);
    }

    onSelectDownType(type) {
        this.download = type;
        console.log('download type :: ', type)
    }

    getSearchLog(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.dashboardservice.getSearchLog(this.search, pageNo, 'branches')
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                console.log('response from search ', response);
                if (response.content.length === 0) {
                    this.booleans.searchStatus = false;
                    this.notification.error('No result Found');
                } else {
                    // console.log('all :: ', response)
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })
    }

    getAllBranch(pageNo) {
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.dashboardservice.getAllBranch(pageNo)
            .subscribe((response) => {
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Branch', error);
            })
    }

    onOpen() {
        $('#CreateModal').modal('show');
    }

    exportDownload() {
        this.search['to'] = this.toDate1;
        this.search['from'] = this.fromDate1;
        this.dashboardservice.exportdownload(this.search, this.download, 'branches')
        /*.subscribe((response) => {
                this.booleans.loadingSearch = false;
                //  window.open(response, "_blank");
                console.log('response from export ', response);
                if(response.content.length === 0){
                    this.booleans.searchStatus= false;
                    this.notification.error('No result Found');
                }else{
                    // console.log('all :: ', response)
                    this.list.allContent = response.content;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })*/
    }
}
