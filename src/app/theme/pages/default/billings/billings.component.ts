import { Component, OnInit } from "@angular/core";
import { BillingsService } from "../../../../../services/api-service/billings.service";
import { NotificationService } from "../../../../../services/notification.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../utils/cache";
import { InstitutionService } from "../../../../../services/api-service/institution.service";

declare const $: any;

@Component({
    selector: 'app-billings',
    templateUrl: './billings.component.html',
    styleUrls: ['billings.component.css']
})
export class BillingsComponent implements OnInit {
    public from = '';
    public to = '';
    public urlId = 0;
    public role = '';
    searchOption = 'Search Type';
    public current: number = 1;
    public dateRangeSearchForm: FormGroup;
    public profile: any;
    public oldObj = [];
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public id = 0;
    public moduleName = 'Billings';
    public searchLabel = '';

    public itemListInstitution = [];
    public selectedItemsInstitution = [];
    public settingsInstitution = {};

    public booleans = {
        loader: false,
        loadingStatus: false,
        loadsearch: false,
        loadingSearch: false,
    };

    loading: boolean;
    toDate: any;
    institution: any;
    fromDate: any;
    toDate1: any;
    fromDate1: any;
    institution1: any;

    endDate: any;
    startDate: any;

    public list = {
        allContent: [],
        dateContent: [],
        roles: [],
        data: [],
        details: [],
        newObj: [],
        oldObj: [],
        updatedObj: [],
    };

    static searchDateRangeForm = () => {
        return {
            agentManagerCode: ['', Validators.compose([])],
            from: ['', Validators.compose([])],
            to: ['', Validators.compose([])],
            agentCode: ['', Validators.compose([])],
            status: ['', Validators.compose([])],
        }
    };

    constructor(private billingsService: BillingsService,
        private notification: NotificationService,
        private institutionservice: InstitutionService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
        this.dateRangeSearchForm = this.fb.group(BillingsComponent.searchDateRangeForm());
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        if (this.role === 'NIBSS') {
            this.onGetAllBillings(0);
        }

        this.settingsInstitution = {
            singleSelection: true,
            text: "All",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
        this.getAllInstitution();

    }

    onOpen() {
        this.toDate1 = null;
        this.fromDate1 = null;
        this.institution1 = null;
        $('#CreateModal').modal('show');
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (!this.booleans.loadsearch) {
            this.onGetAllBillings(this.p - 1);
        } else {
            // this.dateRangeSearchbyLog(this.p - 1)
            this.searchByDate(this.p - 1)
        }

    }

    getAllInstitution() {
        this.itemListInstitution.push({ 'id': '', 'itemName': 'All' });
        this.institutionservice.getAllInstitution(0)
            .subscribe((response) => {
                this.list.allContent = response;
                this.list.allContent.forEach((value) => {
                    this.itemListInstitution.push({
                        "id": value.code,
                        "itemName": value.name
                    });
                });

            },
            error => {
                this.notification.error('Unable to load Institution', error);
            })
    }

    onGetAllBillings(pageNo) {
        this.booleans.loader = true;
        this.booleans.loadsearch = false;
        this.resetToNull();
        this.billingsService.getAllBillings(pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    private resetToNull() {
        this.fromDate = null;
        this.institution = null;
        this.toDate = null;
        this.searchLabel = '';
    }


    exportToCSV() {
        if (!this.toDate1 || !this.fromDate1) {
            this.notification.warning('Select From and To date to proceed!');
            return false;
        } else {
            const fromDate = `${this.fromDate1['year']}-${this.fromDate1['month']}-${this.fromDate1['day']}`;

            const toDate = `${this.toDate1['year']}-${this.toDate1['month']}-${this.toDate1['day']}`;

            const institutionCode = (this.institution1 && this.institution1[0]) ? this.institution1[0]['id'] : '';

            // this.loading = true;
            this.billingsService.exportToCSV(fromDate, toDate, 'download', institutionCode)
            /* .subscribe((response) => {
             console.log('search :: ', response)
             },
             error => {
             console.log('error :: ', error);
             })
             */
            /* this.notification.success('Download successful');
             this.loading = false;
             this.fromDate = null;
             this.toDate = null;
             }, () => {
             this.loading = false;
             this.notification.success('Download successful');
             this.fromDate = null;
             this.toDate = null;
             })*/
        }
    }

    reject() {
        return false;
    }


    // Search by date
    searchByDate(pageNo) {
        this.from = '';
        this.to = '';
        if (!this.toDate && !this.fromDate) {
            this.notification.error('Please, enter a value in the parameter');
        } else if (this.toDate && !this.fromDate) {
            this.notification.error('Please, enter the From date');
        } else {
            this.booleans.loadsearch = true;
            if (this.fromDate) {
                this.to = `${this.toDate['year']}-${this.toDate['month']}-${this.toDate['day']}`;
            }
            if (this.toDate) {
                this.from = `${this.fromDate['year']}-${this.fromDate['month']}-${this.fromDate['day']}`;
            }
            const institutionCode = (this.institution && this.institution[0]) ? this.institution[0]['id'] : '';
            this.booleans.loadingSearch = true;
            const institutionLabel = (this.institution && this.institution[0]) ? this.institution[0]['itemName'] : 'All';
            this.billingsService.getAllBillings(pageNo, this.from, this.to, institutionCode)
                .subscribe((response) => {
                    this.booleans.loadingSearch = false;
                    if (response.content.length === 0) {
                        this.notification.error('No Result Found');
                    }
                    this.booleans.loadsearch = true;
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                    this.p = pageNo + 1;
                    this.searchLabel = `Search result for:|institution: ${institutionLabel} | Date: ${this.from} To  ${this.to}`;
                },
                error => {
                    this.booleans.loadingSearch = false;
                    this.notification.error('Unable to search bills', error);
                })
        }
    }


    dateRangeSearchbyLog(superAgentCode?, status?) {
        this.booleans.loadsearch = true;
        this.dateRangeSearchForm.value['from'] = this.fromDate;
        this.dateRangeSearchForm.value['to'] = this.toDate;
        this.dateRangeSearchForm.value['superAgentCode'] = superAgentCode;
        this.dateRangeSearchForm.value['status'] = status;
        Cache.set('billSearch', this.dateRangeSearchForm.value);
        this.router.navigate(['billing/agent']);
        // this.searchParams = this.dateRangeSearchForm.value;
    }

    onSearchType(searchType) {
        this.searchOption = searchType;
    }

    onSelectType(pageType) {
        this.onGetAllBillings(pageType);
    }

    onItemSelectInstitution(item: any) {
        console.log('item :: ', item);
    }

    OnItemDeSelectInstitution(item: any) {
    }
}
