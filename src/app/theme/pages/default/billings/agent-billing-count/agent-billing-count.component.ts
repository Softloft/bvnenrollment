import { Component, OnInit } from '@angular/core';
import { Cache } from "../../../../../../utils/cache";
import { BillingsService } from "../../../../../../services/api-service/billings.service";
import { NotificationService } from "../../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: 'app-agent-billing-count',
    templateUrl: 'agent-billing-count.component.html',
    styles: []
})
export class AgentBillingCountComponent implements OnInit {
    public p = 1;
    public totalElement = 0;
    public profile = 0;
    public urlId = 0;
    public id = 0;
    public booleans = {
        loader: false,
    };
    public list = {
        allContent: [],
    };


    constructor(private billingsService: BillingsService,
        private notification: NotificationService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.onGetBillSearch(this.p - 1);
    }

    onNavBack() {
        this.router.navigate(['/billings']);
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        this.onGetBillSearch(this.p - 1);
    }

    onGetBillSearch(pageNo) {
        this.booleans.loader = true;
        const agentbillingCount = Cache.get('agentbillingCount');
        console.log('dateRange :: ', agentbillingCount);
        this.billingsService.onGetCount(agentbillingCount.agtCode, agentbillingCount.status, pageNo)
            .subscribe((response) => {
                console.log('response :: ', response)
                this.booleans.loader = false;
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to Search the Agent bill ', error);
            })
    }


}
