import { Component, OnInit } from '@angular/core';
import { BillingsService } from "../../../../../../services/api-service/billings.service";
import { NotificationService } from "../../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../../utils/cache";

declare const $: any;

@Component({
    selector: 'app-billings-agent',
    templateUrl: './billings-agent.component.html',
    styles: []
})
export class BillingsAgentComponent implements OnInit {
    public p = 1;
    public totalElement = 0;
    public profile = 0;
    public urlId = 0;
    public id = 0;
    public booleans = {
        loader: false,
    };
    public list = {
        allContent: [],
    };


    constructor(private billingsService: BillingsService,
        private notification: NotificationService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.onGetBillSearch(this.p - 1);
    }

    onNavBack() {
        this.router.navigate(['/billings']);
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        this.onGetBillSearch(this.p - 1);
    }

    onGetBillSearch(pageNo) {
        this.booleans.loader = true;
        const dateRangeSearchForm = Cache.get('billSearch');
        console.log('dateRange :: ', dateRangeSearchForm);
        this.billingsService.getReportByAgentMgr(dateRangeSearchForm, pageNo)
            .subscribe((response) => {
                console.log('response :: ', response)
                this.booleans.loader = false;
                /*this.totalElement = response.result.totalElements;
                this.list.allContent = response.result.content;*/
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to Search the Agent Manager bill', error);
            })
    }


    public onViewCount(agtCode, status) {
        const agentbillingCount = {
            agtCode: agtCode,
            status: status
        };
        Cache.set('agentbillingCount', agentbillingCount);
        this.router.navigate(['billing/agent-billing']);
    }

    onOpen() {
        $('#CreateModal').modal('show');
    }

}
