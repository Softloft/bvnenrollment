import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillingsComponent } from './billings.component';
import { DefaultComponent } from "../default.component";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { LayoutModule } from "../../../layouts/layout.module";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { BillingsAgentComponent } from './billings-agent/billings-agent.component';
import { BillingsService } from "../../../../../services/api-service/billings.service";
import { AgentBillingCountComponent } from './agent-billing-count/agent-billing-count.component';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": BillingsComponent,
            },
            {
                "path": "agent",
                "component": BillingsAgentComponent,
            },
            {
                "path": "agent-billing",
                "component": AgentBillingCountComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        AngularMultiSelectModule,
        NgbModule,
        PipeModule
    ],
    declarations: [BillingsComponent, BillingsAgentComponent, AgentBillingCountComponent],
    providers: [
        BillingsService
    ]
})
export class BillingsModule { }
