import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListAgentComponent } from "./list-agent.component";
import { RouterModule, Routes } from "@angular/router";
import { DefaultComponent } from "../default.component";
import { AllAgentComponent } from "../all-agent/all-agent.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { NgPureDatatableModule } from "ng-pure-datatable";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LayoutModule } from "../../../layouts/layout.module";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ListAgentComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        NgPureDatatableModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        PipeModule,
        AngularMultiSelectModule,
        FormsModule
    ],
    declarations: [ListAgentComponent]
})
export class ListAgentModule { }