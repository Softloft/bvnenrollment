import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { NotificationService } from "../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../utils/cache";
import { DashboardService } from "../../../../../services/api-service/dashboard.service";
import { BranchService } from "../../../../../services/api-service/branch.service";
import { InstitutionService } from "../../../../../services/api-service/institution.service";

declare const $;
@Component({
    selector: 'app-enrollment-report',
    templateUrl: './enrollment-report.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['enrollment-report.component.css']
})
export class EnrollmentReportComponent implements OnInit {
    public p = 1;
    public param = '';
    public download = '';
    public totalElement = 0;
    public toDate1: any;
    public profile = [];
    public searchOption = 'Date';
    public enrollment = [];
    public toDate: any;
    public fromDate: any;
    public institution: any;
    public institution1: any;
    public status: any;
    public status1: any;
    public fromDate1: any;
    public searchLabel = '';

    public itemListInstitution = [];
    public selectedItemsInstitution = [];
    public settingsInstitution = {};

    public itemListStatus = [
        { id: 'ONGOING', itemName: 'ONGOING' },
        { id: 'SUCCESS', itemName: 'SUCCESS' },
        { id: 'FAILED', itemName: 'FAILED' },
        { id: 'PENDING', itemName: 'PENDING' },
        { id: 'DUPLICATE', itemName: 'DUPLICATE' },
        { id: 'BAD_VERSION', itemName: 'BAD_VERSION' },
        { id: 'BAD_SOURCE', itemName: 'BAD_SOURCE' },
        { id: 'TIMEOUT', itemName: 'TIMEOUT' },
        { id: 'RISK', itemName: 'RISK' },
        { id: 'CANCELLED', itemName: 'CANCELLED' },
        { id: 'UNKNOWN', itemName: 'UNKNOWN' },
    ];
    public selectedItemsStatus = [];
    public settingsStatus = {};

    public search = {
        to: [],
        from: []
    };
    public id = 0;
    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false
    };

    public list = {
        allContent: [],
        data: [],
    };


    constructor(private branchService: BranchService,
        private dashboardservice: DashboardService,
        private institutionservice: InstitutionService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadFunction();
        this.profile = Cache.get('credentials');
        this.enrollment = Cache.get('details');
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.getSearchLog(this.p - 1);
        } else {
            this.getAllEnrollments(this.p - 1);
            // this.getSearchLog(this.p - 1);
        }
    }

    loadFunction() {
        this.settingsInstitution = {
            singleSelection: true,
            text: "All",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
        this.settingsStatus = {
            singleSelection: true,
            text: "Status",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
        this.getAllInstitution();
        this.getAllEnrollments(this.p - 1);
    }

    getAllInstitution() {
        this.itemListInstitution.push({ 'id': '', 'itemName': 'All' });
        this.institutionservice.getAllInstitution(0)
            .subscribe((response) => {
                this.list.allContent = response;
                this.list.allContent.forEach((value) => {
                    this.itemListInstitution.push({
                        "id": value.code,
                        "itemName": value.name
                    });
                });
            },
            error => {
                this.notification.error('Unable to load Institution', error);
            })
    }

    private resetToNull() {
        this.fromDate = null;
        this.toDate = null;
        this.status = null;
        this.institution = null;
        this.searchLabel = '';
    }

    onSelectDownType(type) {
        this.download = type;
    }

    getSearchLog(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        const institutionCode = (this.institution && this.institution[0]) ? this.institution[0]['id'] : '';
        const statusFlag = (this.status && this.status[0]) ? this.status[0]['id'] : '';
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.search['institutionCode'] = institutionCode;
        this.search['status'] = statusFlag;
        this.search['param'] = this.param;
        let fromDate = '';
        let toDate = '';
        if (this.fromDate) {
            fromDate = `${this.fromDate['year']}-${this.fromDate['month']}-${this.fromDate['day']}`;
        }
        if (this.toDate) {
            toDate = `${this.toDate['year']}-${this.toDate['month']}-${this.toDate['day']}`;
        }
        const institutionLabel = (this.institution && this.institution[0]) ? this.institution[0]['itemName'] : 'All';
        this.dashboardservice.getSearchLog(this.search, pageNo, 'enrollments')
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.booleans.searchStatus = false;
                    this.notification.error('No result Found');
                }
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                this.p = pageNo + 1;
                if (this.fromDate) {
                    this.searchLabel = `Search result for:|institution: ${institutionLabel} | Status: ${(statusFlag === '') ? 'All' : statusFlag} | Date: ${fromDate} To  ${toDate}`;
                } else if (this.param) {
                    this.searchLabel = `Search result for:| ticket ID: ${this.param}`;
                }

            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })
    }

    getAllEnrollments(pageNo) {
        this.resetToNull();
        this.p = pageNo + 1;
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.dashboardservice.getAllEnrollment(pageNo)
            .subscribe((response) => {
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Enrollment', error);
            })
    }


    exportDownload() {
        this.search['to'] = this.toDate1;
        this.search['from'] = this.fromDate1;
        this.search['institutionCode'] = (this.institution1 && this.institution1[0]) ? this.institution1[0]['id'] : '';
        this.search['validationStatus'] = (this.status1 && this.status1[0]) ? this.status1[0]['id'] : '';
        this.dashboardservice.exportdownload(this.search, this.download, 'enrollments')
        /*.subscribe((response) => {
         this.booleans.loadingSearch = false;
         //  window.open(response, "_blank");
         if(response.content.length === 0){
         this.booleans.searchStatus= false;
         this.notification.error('No result Found');
         }else{
         this.list.allContent = response.content;
         }
         },
         error => {
         this.booleans.loadingSearch = false;
         this.notification.error('Unable to Search', error)
         })*/
    }

    onOpen() {
        this.status1 = null;
        this.institution1 = null;
        this.toDate1 = null;
        this.fromDate1 = null;
        $('#CreateModal').modal('show');
    }

    onSearch(type, count) {

    }

    onOpenModal(data) {
        this.list.data = data;
        $('#viewModal_1').modal('show');
    }

    onSearchType(type) {
        this.fromDate = null;
        this.toDate = null;
        this.param = '';
        this.searchOption = type;
    }

    onItemSelectInstitution(item: any) {
    }

    OnItemDeSelectInstitution(item: any) {
    }

    onItemSelectStatus(item: any) {
    }

    OnItemDeSelectStatus(item: any) {
    }
}
