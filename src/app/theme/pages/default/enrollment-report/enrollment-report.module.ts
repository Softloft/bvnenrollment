import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnrollmentReportComponent } from './enrollment-report.component';
import { DefaultComponent } from "../default.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { SuccessfulEnrollmentComponent } from './successful-enrollment/successful-enrollment.component';
import { FailedEnrollmentComponent } from './failed-enrollment/failed-enrollment.component';
import { EnrollmentComponent } from './enrollment/enrollment.component';
import { PipeModule } from "../../../../../shared/modules/pipe.module";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": EnrollmentReportComponent
            },
            {
                "path": "enrollment",
                "component": EnrollmentComponent
            },
            {
                "path": "failed-enrollment",
                "component": FailedEnrollmentComponent
            },
            {
                "path": "successful-enrollment",
                "component": SuccessfulEnrollmentComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        AmChartsModule,
        FormsModule,
        PipeModule,
        AngularMultiSelectModule,

    ],
    declarations: [EnrollmentReportComponent, SuccessfulEnrollmentComponent, FailedEnrollmentComponent, EnrollmentComponent]
})
export class EnrollmentReportModule {
}
