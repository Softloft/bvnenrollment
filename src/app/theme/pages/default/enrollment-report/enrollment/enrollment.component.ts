import { Component, OnInit } from '@angular/core';
import { NotificationService } from "../../../../../../services/notification.service";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuditService } from "../../../../../../services/api-service/audit.service";
import { Cache } from "../../../../../../utils/cache";


@Component({
    selector: 'app-enrollment',
    templateUrl: 'enrollment.component.html',
    styles: []
})
export class EnrollmentComponent implements OnInit {
    public urlId = 0;
    public profile: any;
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public id = 0;
    public moduleName = 'All Enrollment';
    public booleans = {
        loader: false,
        loadingStatus: false,
    };

    public list = {
        allContent: [],
        roles: [],
        data: [],
    };


    constructor(private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private auditService: AuditService,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.parentRoute = `For ${Cache.get('audit')}`;
        this.urlId = +this.route.snapshot.paramMap.get('id');
        this.loadFunction();
        this.profile = Cache.get('credentials');
    }

    onNavBack() {
        let previousUrl = Cache.get('previousUrl');
        this.router.navigate([previousUrl]);
    }


    loadFunction() {
        this.onGetAllAudit(this.p - 1);
    }


    getPage(event) {
        this.p = event;
        this.onGetAllAudit(this.p - 1);
    }

    onGetAllAudit(pageNo) {
        this.booleans.loader = true;
        this.auditService.getAllAuditbyId(this.urlId, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                console.log('audit :: ', response);
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

}
