import { Component, OnDestroy, OnInit } from '@angular/core';
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AmChart, AmChartsService } from "@amcharts/amcharts3-angular";
import { NotificationService } from "../../../../../../services/notification.service";
import { DashboardService } from "../../../../../../services/api-service/dashboard.service";
import { UserService } from "../../../../../../services/user.service";


@Component({
    selector: 'app-dashboard-mgr',
    templateUrl: 'dashboard-mgr.component.html',
    styles: []
})
export class DashboardMgrComponent implements OnInit, OnDestroy {
    private chart: AmChart;
    public totalEnrollment = 0;
    public details = [];
    public role = [];
    private branchChart: AmChart;
    private enrolledChart: AmChart;
    public dateRangeSearchForm: FormGroup;
    public userStat = [];
    public branchLga = [];
    itemList = [];
    selectedItems = [];
    settings = {};
    public booleans = {
        loader: false
    }
    static searchDateRangeForm = () => {
        return {
            from: ['', Validators.compose([])],
        }
    };
    public branchList = [];
    public branch = 'Lagos';

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private userservice: UserService,
        private dashboardservice: DashboardService,
        private notification: NotificationService,
        private AmCharts: AmChartsService) {
        this.dateRangeSearchForm = this.fb.group(DashboardMgrComponent.searchDateRangeForm());

    }

    ngOnInit() {
        this.getDashboard();
        this.getAllEnrollments(0);
        this._script.loadScripts('app-index',
            ['assets/app/js/dashboard.js']);

        this.itemList = [
            { "id": 1, "itemName": "Nigeria" },
        ];

        this.selectedItems = [
            { "id": 1, "itemName": "Nigeria" }];
        this.settings = {
            singleSelection: true,
            text: "Select Branch",
            enableSearchFilter: true,
        };

    }

    getDashboard() {
        this.booleans.loader = true;
        this.dashboardservice.getDashboard()
            .subscribe((response) => {
                console.log('response :: ', response);
                this.details = response;
                this.booleans.loader = false;
                this.details['totalEnrollment'] = this.details['pendingEnrollmentCount'] + this.details['successfulEnrollmentCount'] + this.details['failedEnrollmentCount'];
                this.details['countryStatistic'].forEach((value, i) => {
                    this.itemList.push({ "id": i + 2, "itemName": value.location });
                    this.branchList.push({ "branch": value.location, "count": value.count });
                });
                this.userStat = [
                    { "user": "Agent", "count": this.details['agentCount'] },
                    { "user": "Enroller", "count": this.details['enrollerCount'] },
                    { "user": "Branch", "count": this.details['branchCount'] },
                ];
                this.AmCharts.updateChart(this.branchChart, () => {
                    this.branchChart.dataProvider = this.branchList;
                });

                this.AmCharts.updateChart(this.chart, () => {
                    this.chart.dataProvider = this.userStat;
                });

            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })
    }

    getAllEnrollments(pageNo) {
        this.dashboardservice.getAllEnrollment(pageNo)
            .subscribe((response) => {
                console.log('all enrollment :: ', response)
                this.totalEnrollment = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                console.log('errors :: ', error)
            })
    }

    getBranchByLga(id) {
        this.dashboardservice.getBranchByLga(id)
            .subscribe((response) => {
                response.forEach((value) => {
                    this.branchLga.push({ "branch": value.location, "count": value.count });
                });
                this.AmCharts.updateChart(this.branchChart, () => {
                    this.branchChart.dataProvider = this.branchLga;
                });

            },
            error => {
                this.notification.error('Unable to load Lga', error);
            })
    }

    onItemSelect(item: any) {
        if (item['itemName'] === 'Nigeria') {
            this.AmCharts.updateChart(this.branchChart, () => {
                this.branchChart.dataProvider = this.branchList;
            });
        } else {
            this.getBranchByLga(item.itemName);
        }

    }

    OnItemDeSelect(item: any) {
    }

    onSelectAll(items: any) {
    }

    onDeSelectAll(items: any) {
    }


    ngAfterViewInit() {
        this.chart = this.AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "dataProvider": this.userStat,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "count"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "user",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": false
            }

        });

        this.branchChart = this.AmCharts.makeChart("branchChart", {
            "type": "pie",
            "theme": "light",
            "titles": [{
                "text": `Branch`,
                "size": 24
            }],
            "dataProvider": this.branchList,
            "valueField": "count",
            "titleField": "branch",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": true
            }
        })

    }

    ngOnDestroy() {
        if (this.chart) {
            this.AmCharts.destroyChart(this.chart);
        }
        if (this.enrolledChart) {
            this.AmCharts.destroyChart(this.enrolledChart);
        }
        if (this.branchChart) {
            this.AmCharts.destroyChart(this.branchChart);
        }
    }

}