import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AmChart, AmChartsService } from "@amcharts/amcharts3-angular";
import { NotificationService } from "../../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { DashboardService } from "../../../../../../services/api-service/dashboard.service";
import { BranchService } from "../../../../../../services/api-service/branch.service";
import { Cache } from "../../../../../../utils/cache";

declare const $;
@Component({
    selector: 'app-dashboard-enroller',
    templateUrl: 'dashboard-enroller.component.html',
    styles: []
})
export class DashboardEnrollerComponent implements OnInit {
    public p = 1;
    public download = '';
    public totalElement = 0;
    public toDate: any;
    public toDate1: any;
    public profile = [];
    public enrollment = [];
    public fromDate: any;
    public fromDate1: any;
    public search = {
        to: [],
        from: []
    };
    public id = 0;
    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false
    };

    public list = {
        allContent: [],
        data: [],
    };


    constructor(private branchService: BranchService,
        private dashboardservice: DashboardService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadFunction();
        this.profile = Cache.get('credentials');
        this.enrollment = Cache.get('details');
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.getSearchLog(this.p - 1);
        } else {
            this.getAllEnrollments(this.p - 1);
        }
    }

    loadFunction() {
        this.getAllEnrollments(this.p - 1);
    }

    onSelectDownType(type) {
        this.download = type;
        console.log('download type :: ', type)
    }

    getSearchLog(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.dashboardservice.getSearchLog(this.search, pageNo, 'enrollments')
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                console.log('response from search ', response);
                if (response.content.length === 0) {
                    this.booleans.searchStatus = false;
                    this.notification.error('No result Found');
                } else {
                    // console.log('all :: ', response)
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })
    }

    getAllEnrollments(pageNo) {
        this.booleans.searchStatus = false;
        this.dashboardservice.getAllEnrollment(pageNo)
            .subscribe((response) => {
                console.log('all enrollment :: ', response)
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                console.log('errors :: ', error)
                this.notification.error('Unable to load Branch', error);
            })
    }


    exportDownload() {
        this.search['to'] = this.toDate1;
        this.search['from'] = this.fromDate1;
        this.dashboardservice.exportdownload(this.search, this.download, 'enrollments')
        /*.subscribe((response) => {
                this.booleans.loadingSearch = false;
                //  window.open(response, "_blank");
                console.log('response from export ', response);
                if(response.content.length === 0){
                    this.booleans.searchStatus= false;
                    this.notification.error('No result Found');
                }else{
                    // console.log('all :: ', response)
                    this.list.allContent = response.content;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })*/
    }

    onOpen() {
        $('#CreateModal').modal('show');
    }

    onSearch(type, count) {

    }

    onOpenModal(data) {
        this.list.data = data;
        $('#viewModal_1').modal('show');
    }
}
