import { Component, OnDestroy, OnInit } from '@angular/core';
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AmChart, AmChartsService } from "@amcharts/amcharts3-angular";
import { DashboardService } from "../../../../../../services/api-service/dashboard.service";
import { NotificationService } from "../../../../../../services/notification.service";
import { MemberSyncService } from "../../../../../../services/member-sync.service";
import { Cache } from "../../../../../../utils/cache";

@Component({
    selector: 'app-dashboard-nibss',
    templateUrl: 'dashboard-nibss.component.html',
    styles: []
})
export class DashboardNIBSSComponent implements OnInit, OnDestroy {
    private chart: AmChart;
    public details = [];
    public transactionReportdetails = [];
    public role = '';
    private branchChart: AmChart;
    private enrolledChart: AmChart;
    public dateRangeSearchForm: FormGroup;
    public userStat = [];
    public branchLga = [];
    itemList = [];
    selectedItems = [];
    settings = {};
    reportSync = [];
    reportSyncLenght = 0;
    activeUserLenght = 0;
    inActiveUserLenght = 0;
    public booleans = {
        loader: false
    }
    static searchDateRangeForm = () => {
        return {
            from: ['', Validators.compose([])],
        }
    };
    public branchList = [];
    public branch = 'Lagos';

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private dashboardservice: DashboardService,
        private notification: NotificationService,
        private AmCharts: AmChartsService, private memberSync: MemberSyncService) {
        this.dateRangeSearchForm = this.fb.group(DashboardNIBSSComponent.searchDateRangeForm());

    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        // const date = new Date();
        // const today = date.getFullYear() + '-' + ( date.getMonth() + 1 ) + '-' + date.getDate();
        this.getDashboard();
        this.getDashboardTransactionReport();
        this.getMemberSync();
        this._script.loadScripts('app-index',
            ['assets/app/js/dashboard.js']);

        this.itemList = [
            { "id": 1, "itemName": "Nigeria" },
        ];

        this.selectedItems = [
            { "id": 1, "itemName": "Nigeria" }];
        this.settings = {
            singleSelection: true,
            text: "Select Branch",
            enableSearchFilter: true,
        };

    }

    getDashboard() {
        this.booleans.loader = true;
        this.dashboardservice.getDashboard()
            .subscribe((response) => {
                this.details = response;
                this.booleans.loader = false;
                this.details['totalEnrollment'] = this.details['pendingEnrollmentCount'] + this.details['successfulEnrollmentCount'] + this.details['failedEnrollmentCount'] + this.details['otherEnrollmentCount'];
                Cache.set('details', this.details);
                this.details['countryStatistic'].forEach((value, i) => {
                    this.itemList.push({ "id": i + 2, "itemName": value.location });
                    this.branchList.push({ "branch": value.location, "count": value.count });
                });
                this.userStat = [
                    { "user": "NIBSS", "count": this.details['nibssUserCount'] },
                    { "user": "Agent Mgr.", "count": this.details['agentManagerCount'] },
                    { "user": "Agent", "count": this.details['agentCount'] },
                    { "user": "Enroller", "count": this.details['enrollerCount'] },
                    { "user": "Branch", "count": this.details['branchCount'] },
                ];
                this.AmCharts.updateChart(this.branchChart, () => {
                    this.branchChart.dataProvider = this.branchList;
                });

                this.AmCharts.updateChart(this.chart, () => {
                    this.chart.dataProvider = this.userStat;
                });

            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })
    }

    getDashboardTransactionReport() {
        this.booleans.loader = true;
        this.dashboardservice.getDashboardtransactionreport()
            .subscribe((response) => {
                this.transactionReportdetails = response;
            },
            error => {
                // this.notification.error('Unable to load Dashboard', error);
            })
    }

    getBranchByLga(id) {
        this.dashboardservice.getBranchByLga(id)
            .subscribe((response) => {
                response.forEach((value) => {
                    this.branchLga.push({ "branch": value.location, "count": value.count });
                });
                this.AmCharts.updateChart(this.branchChart, () => {
                    this.branchChart.dataProvider = this.branchLga;
                });

            },
            error => {
                this.notification.error('Unable to load Lga', error);
            })
    }

    onItemSelect(item: any) {
        if (item['itemName'] === 'Nigeria') {
            this.AmCharts.updateChart(this.branchChart, () => {
                this.branchChart.dataProvider = this.branchList;
            });
        } else {
            this.getBranchByLga(item.itemName);
        }

    }

    OnItemDeSelect(item: any) {
    }

    onSelectAll(items: any) {
    }

    onDeSelectAll(items: any) {
    }


    ngAfterViewInit() {
        this.chart = this.AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "dataProvider": this.userStat,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "count"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "user",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": false
            }

        });

        this.branchChart = this.AmCharts.makeChart("branchChart", {
            "type": "pie",
            "theme": "light",
            "titles": [{
                "text": `Number of Enrollments by State`,
                "size": 24
            }],
            "dataProvider": this.branchList,
            "valueField": "count",
            "titleField": "branch",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": true
            }
        })

    }

    ngOnDestroy() {
        if (this.chart) {
            this.AmCharts.destroyChart(this.chart);
        }
        if (this.enrolledChart) {
            this.AmCharts.destroyChart(this.enrolledChart);
        }
        if (this.branchChart) {
            this.AmCharts.destroyChart(this.branchChart);
        }
    }

    /*getMemberSync(fromDate?: string, toDate?: string) {
        this.memberSync.getSyncReport(fromDate, toDate).subscribe((response) => {
            console.log('Response: ', response);
            this.reportSync = response;
            this.reportSyncLenght = response.length;
           let active = [], inactive = [];
            this.reportSync.forEach((obj) => {
                (obj['countOfSync'] > 0)?  active.push(obj): inactive.push(obj);
            });
            this.activeUserLenght = active.length;
            this.inActiveUserLenght = inactive.length;

        }, (error) => {
            console.log('Error: ', error);
        })
    }*/

    getMemberSync() {
        this.memberSync.getSyncReportDashboard()
            .subscribe((response) => {
                console.log('Response: ', response);
                this.activeUserLenght = +response.countOfActive;
                this.inActiveUserLenght = +response.countOfInActive;
                this.reportSyncLenght = this.activeUserLenght + this.inActiveUserLenght;
                /* this.reportSync = response;
                 this.reportSyncLenght = response.length;
                let active = [], inactive = [];
                 this.reportSync.forEach((obj) => {
                     (obj['countOfSync'] > 0)?  active.push(obj): inactive.push(obj);
                 });
                 this.activeUserLenght = response.countOfActive;
                 this.inActiveUserLenght = response.countOfActive;
                 */

            }, (error) => {
                console.log('Error: ', error);
            })
    }

}