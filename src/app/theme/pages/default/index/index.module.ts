import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from "../default.component";
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { DashboardMgrComponent } from './dashboard-mgr/dashboard-mgr.component';
import { DashboardAgentComponent } from './dashboard-agent/dashboard-agent.component';
import { DashboardNIBSSComponent } from './dashboard-nibss/dashboard-nibss.component';
import { DashboardEnrollerComponent } from './dashboard-enroller/dashboard-enroller.component';
import { NgxPaginationModule } from "ngx-pagination";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { PipeModule } from "../../../../../shared/modules/pipe.module";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": IndexComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        AmChartsModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        AngularMultiSelectModule,

        NgxPaginationModule,
        BootstrapSwitchModule,
        PipeModule
    ], exports: [
        RouterModule,
    ], declarations: [
        IndexComponent,
        DashboardMgrComponent,
        DashboardAgentComponent,
        DashboardNIBSSComponent,
        DashboardEnrollerComponent
    ]
})
export class IndexModule {



}