import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../../../../services/user.service";
import { Cache } from "../../../../../utils/cache";

@Component({
    selector: "app-index",
    templateUrl: "./index.component.html",
    styleUrls: ['index.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit {
    public role = '';
    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private userservice: UserService,
        private AmCharts: AmChartsService) { }

    ngOnInit() {
        this._script.loadScripts('app-index',
            ['assets/app/js/dashboard.js']);
        this.role = Cache.get('credentials')['user']['userType'];
    }


    getDashboard() {

    }
}