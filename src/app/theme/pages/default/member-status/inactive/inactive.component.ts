import { Component, OnInit } from '@angular/core';
import { NotificationService } from "../../../../../../services/notification.service";
import { MemberSyncService } from "../../../../../../services/member-sync.service";
import { SearcherService } from "../../../../../../services/searcher.service";
import { Cache } from "../../../../../../utils/cache";
import { DashboardService } from "../../../../../../services/api-service/dashboard.service";

declare const $;

@Component({
    selector: 'app-inactive',
    templateUrl: 'inactive.component.html',
    styles: []
})
export class InactiveComponent implements OnInit {
    public searchOption = 'Date';
    public modalType = 'Date';
    public searchtext = '';
    public param = '';
    public activeStatus = false;
    public urlId = 0;
    public toDate: any;
    public fromDate: any;

    public role = '';
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public id = 0;
    public booleans = {
        loadingSearch: false,
        searchStatus: false,
        loader: false,
    };

    public list = {
        allContent: [],
        roles: [],
        tasks: [],
    };

    public search = {
        to: '',
        from: ''
    };
    constructor(private notification: NotificationService,
        private dashboardservice: DashboardService,
        private syncService: MemberSyncService,
        private searchservice: SearcherService, ) {
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        if (this.role === 'NIBSS') {
            this.list.tasks = Cache.get('tasks');
            this.loadFunction();
        }
    }


    loadFunction() {
        this.onGetActiveSync(this.p - 1);
    }

    onExport() {
        this.modalType = 'export'
        this.onOpen();
    }

    onOpen() {
        this.toDate = '';
        this.fromDate = '';
        this.param = '';
        $('#CreateModal').modal('show');
    }

    exportDownload() {
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.search['param'] = this.param;
        this.dashboardservice.exportReport(this.search, 'sync-report', 'false');
    }

    onGetActiveSync(pageNo) {
        this.booleans.loader = true;
        let searchData = {
            from: '',
            to: '',
            username: '',
        };
        this.syncService.getActiveSync(pageNo, this.activeStatus, searchData)
            .subscribe((response) => {
                console.log('active response :: ', response)
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Sync Report', error);
            });
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.onGetActiveSync(this.p - 1);
        } else {
            this.searchMethod(this.p - 1);
        }
    }


    onSearchType(type) {
        this.searchtext = '';
        this.fromDate = null;
        this.toDate = null;
        this.searchOption = type;
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        let searchData = {
            from: this.fromDate,
            to: this.toDate,
            username: this.searchtext,
        };
        this.syncService.getActiveSync(pageNo, this.activeStatus, searchData)
            .subscribe((response) => {
                console.log('response search:: ', response);
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.toDate = '';
                    this.fromDate = '';
                    this.searchtext = '';
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }
}
