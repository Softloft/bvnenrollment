import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MemberSyncService } from "../../../../../../services/member-sync.service";
import { Cache } from "../../../../../../utils/cache";
import { NotificationService } from "../../../../../../services/notification.service";
import { SearcherService } from "../../../../../../services/searcher.service";

@Component({
    selector: 'app-filter-super-agent',
    templateUrl: './filter-super-agent.component.html',
    styleUrls: ['./filter-super-agent.component.css']
})
export class FilterSuperAgentComponent implements OnInit {
    public searchOption = 'Date';
    public searchtext = '';
    public activeStatus = true;
    public urlId = 0;
    public code = '';
    public toDate: any;
    public fromDate: any;

    public role = '';
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public id = 0;
    public booleans = {
        loadingSearch: false,
        searchStatus: false,
        loader: false,
    };

    public list = {
        allContent: [],
        roles: [],
        tasks: [],
    };

    public searchData = {
        from: '',
        to: '',
        username: '',
    }

    constructor(private notification: NotificationService,
        private syncService: MemberSyncService,
        private router: Router,
        private searchservice: SearcherService, ) {
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        console.log('active role :: ', this.role)
        if (this.role === 'NIBSS') {
            this.list.tasks = Cache.get('tasks');
            this.loadFunction();
            console.log('active user')
        }
    }


    loadFunction() {
        const navDetails = Cache.get('agentMgrCode');
        this.code = navDetails.code;
        this.searchData.to = navDetails.to;
        this.searchData.from = navDetails.from;
        this.searchData.username = navDetails.param;

        this.onGetActiveSync(this.p - 1, this.searchData);
    }


    onGetActiveSync(pageNo, searchData) {
        this.booleans.loader = true;
        this.syncService.getActiveAgentMgr(pageNo, this.activeStatus, searchData, this.code)
            .subscribe((response) => {
                console.log('active response :: ', response)
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Sync Report', error);
            });
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.onGetActiveSync(this.p - 1, this.searchData);
        } else {
            this.searchMethod(this.p - 1);
        }
    }


    onSearchType(type) {
        this.searchOption = type;
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.searchData.to = this.toDate;
        this.searchData.from = this.fromDate;
        this.searchData.username = this.searchtext;
        this.syncService.getActiveAgentMgrSync(pageNo, this.activeStatus, this.code, this.searchData)
            .subscribe((response) => {
                console.log('response search:: ', response);
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.toDate = '';
                    this.fromDate = '';
                    this.searchtext = '';
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

}
