import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterSuperAgentComponent } from "./filter-super-agent.component";
import { DefaultComponent } from "../../default.component";
import { RouterModule, Routes } from "@angular/router";
import { NgxPaginationModule } from "ngx-pagination";
import { LayoutModule } from "../../../../layouts/layout.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PipeModule } from "../../../../../../shared/modules/pipe.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": FilterSuperAgentComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        NgxPaginationModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        PipeModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        FilterSuperAgentComponent
    ]
})
export class FilterSuperAgentModule { }
