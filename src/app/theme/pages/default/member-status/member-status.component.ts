import { Component, OnInit } from '@angular/core';
import { MemberSyncService } from "../../../../../services/member-sync.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../utils/cache";
import { NotificationService } from "../../../../../services/notification.service";

@Component({
    selector: 'app-member-status',
    templateUrl: './member-status.component.html',
    styleUrls: ['./member-status.component.css']
})
export class MemberStatusComponent implements OnInit {
    fromDate: any;
    toDate: any;
    reportSync: any;
    fromDate_: any;
    toDate_: any;
    loader: any;
    status = true;

    constructor(private memberSync: MemberSyncService,
        private activatedRoute: ActivatedRoute,
        private notification: NotificationService,
        private router: Router) {

    }

    ngOnInit() { }

    /*getMemberSync(fromDate?: string, toDate?: string) {
        this.loader = true;
        this.memberSync.getSyncReport(fromDate, toDate).subscribe((response) => {
            this.reportSync = response;
            this.loader = false;
        }, (error) => {
            this.loader = false;
            this.notification.error('Unable to load Sync Report', error);
            console.log('Error: ', error);
        })
    }
*/
    setDateRequired() {
        this.fromDate_ = `${this.fromDate['year']}-${this.fromDate['month']}-${this.fromDate['day']}`;
        this.toDate_ = `${this.toDate['year']}-${this.toDate['month']}-${this.toDate['day']}`;
        //    this.getMemberSync(this.fromDate_, this.toDate_);
    }

    reject() {
        return false;
    }

    fetchData() {
        this.setDateRequired();
    }

    preparePath() {
        sessionStorage.setItem('fromDate', this.fromDate_);
        sessionStorage.setItem('toDate', this.toDate_);
    }

    onSwitch(bools) {
        this.status = bools;
    }
}
