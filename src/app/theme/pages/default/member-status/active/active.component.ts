import { Component, OnInit } from '@angular/core';
import { SearcherService } from "../../../../../../services/searcher.service";
import { NotificationService } from "../../../../../../services/notification.service";
import { Cache } from "../../../../../../utils/cache";
import { MemberSyncService } from "../../../../../../services/member-sync.service";
import { Router } from "@angular/router";
import { DashboardService } from "../../../../../../services/api-service/dashboard.service";

declare const $;

@Component({
    selector: 'app-active',
    templateUrl: 'active.component.html',
    styles: []
})
export class ActiveComponent implements OnInit {
    public searchOption = 'Date';
    public searchtext = '';
    public param = '';
    public modalType = '';
    public activeStatus = true;
    public urlId = 0;
    public toDate: any;
    public fromDate: any;

    public role = '';
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public id = 0;
    public search = {
        to: '',
        from: ''
    };

    public booleans = {
        loadingSearch: false,
        searchStatus: false,
        loader: false,
    };

    navDetails = {
        code: '',
        to: this.toDate,
        from: this.fromDate,
        param: this.searchtext
    };

    public list = {
        allContent: [],
        roles: [],
        tasks: [],
    };

    constructor(private notification: NotificationService,
        private syncService: MemberSyncService,
        private dashboardservice: DashboardService,
        private router: Router,
        private searchservice: SearcherService, ) {
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        if (this.role === 'NIBSS') {
            this.list.tasks = Cache.get('tasks');
            this.loadFunction();
        }
    }


    loadFunction() {
        this.onGetActiveSync(this.p - 1);
    }

    onExport() {
        this.modalType = 'export'
        this.onOpen();
    }

    onOpen() {
        this.toDate = '';
        this.fromDate = '';
        this.param = '';
        $('#CreateModal').modal('show');
    }

    exportDownload() {
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.search['param'] = this.param;
        this.dashboardservice.exportReport(this.search, 'sync-report', 'true');
    }

    onGetActiveSync(pageNo) {
        this.booleans.loader = true;
        let searchData = {
            from: '',
            to: '',
            username: '',
        };
        this.syncService.getActiveSync(pageNo, this.activeStatus, searchData)
            .subscribe((response) => {
                console.log('active response :: ', response)
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Sync Report', error);
            });
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.onGetActiveSync(this.p - 1);
        } else {
            this.searchMethod(this.p - 1);
        }
    }


    onSearchType(type) {
        this.searchtext = '';
        this.fromDate = null;
        this.toDate = null;

        this.searchOption = type;
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        let searchData = {
            from: this.fromDate,
            to: this.toDate,
            username: this.searchtext,
        };
        this.syncService.getActiveSync(pageNo, this.activeStatus, searchData)
            .subscribe((response) => {
                console.log('response search:: ', response);
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.navDetails.to = this.toDate;
                    this.navDetails.from = this.fromDate;
                    this.navDetails.param = this.searchtext;
                    this.toDate = '';
                    this.fromDate = '';
                    this.searchtext = '';
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    onGetAgentMgr(code) {
        this.navDetails.code = code;
        Cache.set('agentMgrCode', this.navDetails);
        this.router.navigate(['/sync-agentMgr']);
        this.booleans.loader = true;
    }
}
