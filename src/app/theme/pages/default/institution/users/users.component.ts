import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { NIBSSService } from "../../../../../../services/api-service/nibss.service";
import { Router } from "@angular/router";
import { NotificationService } from "../../../../../../services/notification.service";
import { EventsService as PagingEvent } from "larang-paginator";
import { environment } from "../../../../../../environments/environment";
import { ConfigService } from "../../../../../../services/api-service/config.service";
import { StateService } from "../../../../../../services/api-service/state.service";
import { Cache } from "../../../../../../utils/cache";
import { SearcherService } from "../../../../../../services/searcher.service";
import { UserService } from "../../../../../../services/user.service";
import { InstitutionService } from "../../../../../../services/api-service/institution.service";
import { defaultHostName } from "../../../../../../utils/check-hostname";

declare const $;
declare var swal: any;

@Component({
    selector: 'app-users',
    templateUrl: 'users.component.html',
    styles: []
})
export class UsersComponent implements OnInit {
    public id = 0;
    public searchtext = '';
    public status = '';

    public lgaId = null;
    public searchOption = 'Search Type';
    public userType = '';
    public p = 1;
    public role = '';
    public totalElement = 0;
    url: string = '/nibss';
    createForm: FormGroup;
    public moduleName = 'Institution';
    public list = {
        tasks: [],
        data: [],
        allContent: []
    };

    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false,

        loadRole: false,
        stateStatus: false,
        lgaStatus: false,
        cityStatus: false,
        loadingStatus: false,
        emptyState: false,
        emptyLga: false,
        showUpdate: false,
        emptyCity: false,
        phoneLimit: false,
        showbvnstatus: false,
        loadsearch: false,
    }

    formData = function() {
        return {
            reason: ['', [Validators.compose([Validators.required])]],
        }
    };

    public paginator = {
        path: defaultHostName() + environment.API_URL.user + this.url,
        limit: 10,
        viewPage: 'pageNumber',
        paginate: 'pageSize',
        perNav: 10,
        data: null,
        from: 'index_paging'

    };


    constructor(private nibssService: NIBSSService,
        private configservice: ConfigService,
        private searchservice: SearcherService,
        private institutionservice: InstitutionService,
        private stateService: StateService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private userservice: UserService,
        private pagingEvent: PagingEvent) {
        this.createForm = this.fb.group(this.formData());
        this.pagingEvent.on(this.paginator.from, (response) => {
            this.paginator.data = response.data;
        });
    }

    ngOnInit() {
        const getRole = Cache.get('credentials')['user']['role']['name'];
        if (getRole.toLowerCase() === 'report viewer') {
            return;
        }
        switch (this.router.url) {
            case '/institution/pending':
                this.status = 'pending';
                break;
            case '/institution/approved':
                this.status = 'approved';
                break;
            case '/institution/disapproved':
                this.status = 'disapproved';
                break;
        }
        this.userType = Cache.get('credentials')['makerCheckerType'];
        this.role = Cache.get('credentials')['user']['userType'];
        if (this.role === 'NIBSS') {
            this.list.tasks = Cache.get('tasks');
            this.getAllStatus(this.p - 1);
        }
    }


    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.searchMethod(this.p - 1);
        } else {
            this.getAllStatus(this.p - 1);
        }
    }


    onApprove(data) {
        const body = { id: data.id };
        this.institutionservice.approve(body, 'institution')
            .subscribe((response) => {
                this.list.allContent.forEach((val, i) => {
                    if (val.id === body['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.notification.success('Institution was successfully activated');
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }


    onDisapprove() {
        const body = {
            id: this.id,
            reason: this.createForm.value['reason']
        }
        this.institutionservice.disapprove(body, 'institution')
            .subscribe((response) => {
                console.log('response from disapproval :: ', response);
                this.createForm.reset();
                $('#CreateModal').modal('hide');
                this.notification.success('Disapproval was successful');
                this.list.allContent.forEach((val, i) => {
                    if (val.id === body.id) {
                        this.list.allContent.splice(i, 1);
                    }
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to disapproved request', error);
            })
    }


    onView(data) {
        this.list.data = data;
        $('#viewModal_1').modal('show');
    }

    onOpen(data) {
        this.id = data.id
        $('#CreateModal').modal('show');
    }


    getAllStatus(pageNo) {
        this.booleans.loader = true;
        this.booleans.searchStatus = false;
        this.nibssService.getAllStatus(pageNo, this.status, 'institution/').subscribe(
            (response) => {
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            }
        )
    }


    onSearchType(searchType) {
        this.searchOption = searchType;
    }


    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.searchservice.userSearch(this.searchtext, 'institution/approval/' + this.status, pageNo)
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }
}
