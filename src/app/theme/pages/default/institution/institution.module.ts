import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LayoutModule } from "../../../layouts/layout.module";
import { RouterModule, Routes } from "@angular/router";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { DefaultComponent } from "../default.component";
import { NgPureDatatableModule } from "ng-pure-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { InstitutionComponent } from "./institution.component";
import { UsersComponent } from "./users/users.component";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": InstitutionComponent,
            },
            {
                'path': 'pending',
                'component': UsersComponent
            },
            {
                'path': 'approved',
                'component': UsersComponent
            },
            {
                'path': 'disapproved',
                'component': UsersComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        NgPureDatatableModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        PipeModule,
        AngularMultiSelectModule,
        FormsModule
    ],
    declarations: [InstitutionComponent, UsersComponent]
})
export class InstitutionModule { }
