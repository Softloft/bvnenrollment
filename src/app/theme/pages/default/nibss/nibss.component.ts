import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { NIBSSService } from "../../../../../services/api-service/nibss.service";
import { Router } from "@angular/router";
import { NotificationService } from "../../../../../services/notification.service";
import { EventsService as PagingEvent } from "larang-paginator";
import { environment } from "../../../../../environments/environment";
import { ConfigService } from "../../../../../services/api-service/config.service";
import { StateService } from "../../../../../services/api-service/state.service";
import { Cache } from "../../../../../utils/cache";
import { DashboardService } from "../../../../../services/api-service/dashboard.service";
import { SearcherService } from "../../../../../services/searcher.service";
import { UserService } from "../../../../../services/user.service";
import { defaultHostName } from "../../../../../utils/check-hostname";

declare const $;
declare var swal: any;

@Component({
    selector: 'app-nibss',
    templateUrl: './nibss.component.html',
    styles: []
})

export class NIBSSComponent implements OnInit {
    public id = 0;
    public searchtext = '';
    public roleStatus = '';

    public lgaId = null;
    public searchOption = 'Search Type';
    itemListRole = [];
    selectedItemsRole = [];
    settingsRole = {};

    itemListState = [];
    selectedItemsState = [];
    settingsState = {};

    itemListLga = [];
    selectedItemsLga = [];
    settingsLga = {};
    title = 'app';
    public p = 1;
    public role = '';
    public userRole = '';
    public status = '';
    public totalElement = 0;
    url: string = '/nibss';
    createForm: FormGroup;
    public moduleName = 'NIBSS';
    public list = {
        roles: [],
        tasks: [],
        phoneArray: 0,
        data: [],
        allState: [],
        allLga: [],
        allCity: [],
        allContent: []
    };

    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false,

        loadRole: false,
        stateStatus: false,
        lgaStatus: false,
        cityStatus: false,
        loadingStatus: false,
        emptyState: false,
        emptyLga: false,
        showUpdate: false,
        emptyCity: false,
        phoneLimit: false,
        showbvnstatus: false,
        loadsearch: false,
    }
    public feedBack = {
        allResponse: [],
        viewDetails: [],
        moduleName: 'NIBSS',
        formType: 'Create',
        loader: false,
        submitStatus: false,
        showUpdateButton: false
    };

    formData = function() {
        return {
            bvn: ['', [Validators.minLength(11), Validators.maxLength(11)]],
            state: ['', Validators.compose([Validators.required])],
            city: ['', Validators.compose([Validators.required])],
            lgaId: ['', Validators.compose([Validators.required])],
            username: ['', Validators.compose([Validators.required])],
            emailAddress: ['', Validators.compose([Validators.required, Validators.email])],
            name: new FormGroup({
                firstName: new FormControl('', Validators.required),
                lastName: new FormControl('', Validators.required),
                middleName: new FormControl(''),
                title: new FormControl('', Validators.required),
            }),
            phoneList: new FormArray([]),
            roleId: ['', Validators.compose([Validators.required])],
            streetNumber: ['', Validators.compose([])],
            staffNumber: ['', Validators.compose([Validators.required])],
            userAuthorisationType: ['', Validators.compose([])],
            userId: ['', Validators.compose([])],
        }
    };

    public paginator = {
        path: defaultHostName() + environment.API_URL.user + this.url,
        limit: 10,
        viewPage: 'pageNumber',
        paginate: 'pageSize',
        perNav: 10,
        data: null,
        from: 'index_paging'

    };


    constructor(private nibssService: NIBSSService,
        private configservice: ConfigService,
        private searchservice: SearcherService,
        private stateService: StateService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private userservice: UserService,
        private pagingEvent: PagingEvent) {
        this.createForm = this.fb.group(this.formData());
        this.pagingEvent.on(this.paginator.from, (response) => {
            this.paginator.data = response.data;
        });
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        const getRole = Cache.get('credentials')['user']['role']['name'];
        if (getRole && getRole !== '') {
            this.userRole = getRole.toLowerCase();
        }

        this.status = Cache.get('credentials')['makerCheckerType'];
        if (this.role === 'NIBSS') {
            this.list.tasks = Cache.get('tasks');
            this.getState();
            this.onAddPhoneList();
            this.getAllNIBSSUsers(this.p - 1);
            this.onGetActivatedRole();
        }
    }

    onChangeBvn(event) {
        if (+event.target.value.length === 11) {
            this.booleans.showbvnstatus = false;
        } else {
            this.booleans.showbvnstatus = true;
        }
    }


    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.searchMethod(this.p - 1);
        } else {
            this.getAllNIBSSUsers(this.p - 1);
        }
    }

    onGetActivatedRole() {
        this.booleans.loadRole = true;
        this.configservice.getActivatedRole('NIBSS')
            .subscribe((response) => {
                this.list.roles = response;
                this.booleans.loadRole = false;
                this.list.roles.forEach((value) => {
                    this.itemListRole.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.fetchRole();
            },
            error => {
                this.booleans.loadRole = false;
                this.notification.error('Unable to load Role', error);
            })
    }

    onEdit(data) {
        this.getLga(data.user.contactDetails.lga.state.id);
        // this.selectedItemsRole = [{'id': data.user.contactDetails.lga.state.id, 'itemName': data.user.contactDetails.lga.state.name}]
        this.lgaId = [{ 'id': data.user.contactDetails.lga.id, 'itemName': data.user.contactDetails.lga.name }];
        const formdata = function() {
            return {
                staffNumber: [data.staffNumber, Validators.compose([Validators.required])],
                additionalInfo1: [data.additionalInfo1, Validators.compose([])],
                additionalInfo2: [data.additionalInfo2, Validators.compose([])],
                bvn: [data.user.bvn, Validators.compose([Validators.minLength(11), Validators.maxLength(11)])],
                city: [data.user.contactDetails.city, Validators.compose([Validators.required])],
                lgaId: [[{
                    'id': data.user.contactDetails.lga.id,
                    'itemName': data.user.contactDetails.lga.name
                }], Validators.compose([Validators.required])],
                state: [[{
                    'id': data.user.contactDetails.lga.state.id,
                    'itemName': data.user.contactDetails.lga.state.name
                }], Validators.compose([])],
                roleId: [[{
                    'id': data.user.role.id,
                    'itemName': data.user.role.name
                }], Validators.compose([Validators.required])],
                username: [data.user.username, Validators.compose([])],
                emailAddress: [data.user.emailAddress, Validators.compose([Validators.required, Validators.email])],
                userAuthorisationType: [data.makerCheckerType, Validators.compose([])],
                streetNumber: [data.user.contactDetails['streetNumber'], Validators.compose([])],
                userId: [data.user.id, Validators.compose([])],
                id: [data.id, Validators.compose([])],
                name: new FormGroup({
                    firstName: new FormControl(data.user.name.firstName, Validators.required),
                    lastName: new FormControl(data.user.name.lastName, Validators.required),
                    middleName: new FormControl(data.user.name.middleName),
                    title: new FormControl(data.user.name.title, Validators.required),
                }),
                phoneList: new FormArray([]),
            }
        };
        this.createForm = this.fb.group(formdata());
        data.user.phoneList.forEach(val => {
            const control = new FormControl(val);
            (<FormArray>this.createForm.get('phoneList')).push(control);
        });
        this.booleans.showUpdate = true;
        this.booleans.emptyCity = false;
        this.booleans.emptyLga = false;
        this.booleans.emptyState = false;
        $('#CreateModal').modal('show');
    }


    onView(data) {
        this.list.data = data;
        $('#viewModal_1').modal('show');
    }

    onOpen() {
        this.roleStatus = '';
        this.list.allLga = [];
        this.booleans.showUpdate = false;
        this.booleans.emptyCity = false;
        this.booleans.emptyLga = false;
        this.booleans.emptyState = false;
        this.booleans.showUpdate = false;
        this.createForm = this.fb.group(this.formData());
        this.onAddPhoneList();
        this.createForm.reset();
        $('#CreateModal').modal('show');
    }

    closeModal() {
        $('#openModal').modal('hide');
        this.feedBack.formType = 'Create';
        this.feedBack.showUpdateButton = false;
    }


    getAllNIBSSUsers(pageNo) {
        this.booleans.loader = true;
        this.booleans.searchStatus = false;
        this.nibssService.getAllNIBSSUsers(pageNo).subscribe(
            (response) => {
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                this.booleans.loader = false;
                this.notification.error('Unable to load NIBSS ', error);
            }
        )
    }

    getState() {
        this.booleans.stateStatus = true;
        this.stateService.getState().subscribe(
            (response) => {
                this.list.allState = response;
                this.list.allState.forEach((value) => {
                    this.itemListState.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.fetchState();
                if (this.list.allState.length === 0) {
                    this.booleans.emptyState = true;
                }
                this.booleans.stateStatus = false;
            },
            (error) => {
                this.booleans.stateStatus = false;
                this.notification.error('Unable load states ', error);
            }
        )
    }

    getLga(id) {
        //  const id = event.target.value;
        this.booleans.lgaStatus = true;
        this.stateService.getLga(id).subscribe(
            (response) => {
                this.list.allLga = response;
                this.list.allLga.forEach((value) => {
                    this.itemListLga.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.fetchLga();
                if (this.list.allLga.length === 0) {
                    this.booleans.emptyLga = true;
                }
                this.booleans.lgaStatus = false;
            },
            (error) => {
                this.booleans.lgaStatus = false;
                this.notification.error('Unable load Lga ', error);
            }
        )
    }

    getCity(event) {
        const id = event.target.value;
        this.booleans.cityStatus = true;
        this.stateService.getCity(id).subscribe(
            (response) => {
                this.list.allCity = response;
                if (this.list.allCity.length === 0) {
                    this.booleans.emptyCity = true;
                }
                this.booleans.cityStatus = false;
            },
            (error) => {
                this.booleans.cityStatus = false;
                this.notification.error('Unable load City ', error);
            }
        )
    }


    insertForm(action, data?: any) {
        if (this.roleStatus === 'report viewer') {
            this.createForm.value['userAuthorisationType'] = 'OPERATOR';
        }
        if (!Number.isInteger(+this.createForm.value['bvn'])) {
            return this.notification.error('BVN must be a number');
        } {
            this.createForm.value['phoneList'].forEach(value => {
                if (!Number.isInteger(+value)) {
                    return this.notification.error('Phone must a number');
                }
            });
        }
        if (typeof this.createForm.value['lgaId'] === 'object') {
            this.createForm.value['lgaId'] = this.createForm.value['lgaId'][0]['id'];
            this.createForm.value['state'] = this.createForm.value['state'][0]['id'];
            this.createForm.value['roleId'] = this.createForm.value['roleId'][0]['id'];
        }
        if (action === 'update') {
            this.list.data = this.createForm.value;
            this.post(action);
        } else if (action === 'toggle') {
            this.list.data = data;
            this.post(action);
        } else if (action === 'new') {
            this.list.data = this.createForm.value;
            this.post(action);
        }

    }

    private post(action) {
        this.booleans.loadingStatus = true;
        this.nibssService.insertForm(this.list.data, action)
            .subscribe((response) => {
                this.booleans.loadingStatus = false;
                $('#CreateModal').modal('hide');
                this.list.allLga = [];
                this.createForm.reset();
                if (action === 'update' || action === 'toggle') {
                    this.list.allContent.forEach((val, i) => {
                        if (val.id === this.list.data['id']) {
                            this.list.allContent[i] = response;
                        }
                    });
                    if (action === 'toggle') {
                        return this.notification.success('Status was successfully changed');
                    }
                } else {
                    this.list.allContent.unshift(response);
                }
                this.roleStatus = '';
                this.notification.success('Status was successfully changed');

            },
            error => {
                this.booleans.loadingStatus = false;
                this.notification.error(`Unable to ${action} ${this.moduleName}`, error)
            });
    }

    onAddPhoneList() {
        const control = new FormControl(null);
        (<FormArray>this.createForm.get('phoneList')).push(control);
        this.list.phoneArray = (<FormArray>this.createForm.get('phoneList')).length;
        if (+this.list.phoneArray >= 3) {
            this.booleans.phoneLimit = true;
        }
    }

    onRemovePhone(i) {
        (<FormArray>this.createForm.get('phoneList')).removeAt(i);
        this.list.phoneArray = (<FormArray>this.createForm.get('phoneList')).length;
        if (+this.list.phoneArray < 3) {
            this.booleans.phoneLimit = false;
        }
    }

    insert() {
        if (!this.booleans.showbvnstatus) {
            if (!this.booleans.showUpdate) {
                this.insertForm('new');
            } else if (this.booleans.showUpdate) {
                this.insertForm('update');
            }
        } else {
            this.showAlert();
        }
    }

    showAlert() {
        swal({
            title: 'BVN must be 11 digit',
            text: "",
            type: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay'
        });
    }

    /*createNIBSSUser() {
        this.nibssService.createNIBSSUser(this.createForm.value)
            .subscribe(
                (response) => {
                    this.feedBack.allResponse.unshift(response);
                    (this.feedBack.allResponse.length === 0) ? this.feedBack.allResponse.push(response) : this.feedBack.allResponse.unshift(response);
                    this.callBackFunction();
                }
            )
    }


    private callBackFunction() {
        this.feedBack.submitStatus = false;
        this.createForm.reset();
        this.closeModal();
    }*/

    fetchRole() {
        this.settingsRole = {
            singleSelection: true,
            text: "Select Role",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }

    onItemSelectRole(item: any) {
        console.log('selected :: ', item.itemName.toLowerCase());
        this.roleStatus = item.itemName.toLowerCase();
        this.selectedItemsRole = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectRole(item: any) {
    }

    onSelectAllRole(items: any) {
    }

    onDeSelectAllRole(items: any) {
    }

    fetchLga() {
        this.settingsLga = {
            singleSelection: true,
            text: "Select Lga",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }

    onItemSelectLga(item: any) {
        this.selectedItemsLga = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectLga(item: any) {
    }

    onSelectAllLga(items: any) {
    }

    onDeSelectAllLga(items: any) {
    }

    fetchState() {
        this.settingsState = {
            singleSelection: true,
            text: "Select State",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }

    onItemSelectState(item: any) {
        this.list.allLga = [];
        this.itemListLga = [];
        this.lgaId = null;
        this.selectedItemsState = [{ 'id': item.id, 'itemName': item.itemName }];
        this.getLga(item.id);
    }

    OnItemDeSelectState(item: any) {
    }

    onSelectAllState(items: any) {
    }

    onDeSelectAllState(items: any) {
    }

    onAudit(data) {
        Cache.set('previousUrl', this.router.url);
        Cache.set('audit', `${data.user.name.firstName} ${data.user.name.lastName}`);
        this.router.navigate(['audit/', data.id]);
    }

    onSearchType(searchType) {
        this.searchOption = searchType;
    }


    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.searchservice.databaseSearch(this.searchtext, 'nibss', pageNo)
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }
}
