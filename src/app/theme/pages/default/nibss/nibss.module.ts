import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NIBSSComponent } from './nibss.component';
import { DefaultComponent } from '../default.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { LarangPaginatorModule } from 'larang-paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';
import { NgxPaginationModule } from "ngx-pagination";
import { Select2Module } from "ng2-select2";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { UsersComponent } from './users/users.component';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': NIBSSComponent
            },
            {
                'path': 'pending',
                'component': UsersComponent
            },
            {
                'path': 'approved',
                'component': UsersComponent
            },
            {
                'path': 'disapproved',
                'component': UsersComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        BootstrapSwitchModule,
        NgbModule,
        CommonModule,
        Select2Module,
        NgxPaginationModule,
        RouterModule.forChild(routes),
        LayoutModule,
        LarangPaginatorModule,
        ReactiveFormsModule,
        AngularMultiSelectModule,
        PipeModule,
        FormsModule
    ],
    declarations: [NIBSSComponent, UsersComponent]
})
export class NIBSSModule { }
