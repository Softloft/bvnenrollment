import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgentManagersComponent } from './agent-managers.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LayoutModule } from "../../../layouts/layout.module";
import { RouterModule, Routes } from "@angular/router";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { DefaultComponent } from "../default.component";
import { AgentComponent } from './agent/agent.component';
import { NgPureDatatableModule } from "ng-pure-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { AllAgentComponent } from '../all-agent/all-agent.component';
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { UsersComponent as AgentUsersComponent } from "./agent/users/users.component";
import { UsersComponent } from "./users/users.component";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": AgentManagersComponent,
            },
            {
                "path": "all",
                "component": AgentComponent,
            },
            {
                'path': 'pending',
                'component': AgentUsersComponent
            },
            {
                'path': 'approved',
                'component': AgentUsersComponent
            },
            {
                'path': 'disapproved',
                'component': AgentUsersComponent
            },
            {
                "path": ":id",
                "component": AgentComponent,
            },
            {
                'path': 'mgr/pending',
                'component': UsersComponent
            },
            {
                'path': 'mgr/approved',
                'component': UsersComponent
            },
            {
                'path': 'mgr/disapproved',
                'component': UsersComponent
            },


        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        NgPureDatatableModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        PipeModule,
        AngularMultiSelectModule,
        FormsModule
    ],
    declarations: [AgentManagersComponent, AgentComponent, UsersComponent, AgentUsersComponent]
})
export class AgentManagersModule { }
