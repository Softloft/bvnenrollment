import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UserService } from "../../../../../services/user.service";
import { PasswordService } from "../../../../../services/password.service";
import { EventsService } from "../../../../../services/event.service";
import { NotificationService } from "../../../../../services/notification.service";
import { Cache } from "../../../../../utils/cache";

declare let mLayout: any;
declare const $;

@Component({
    selector: 'app-password-setting',
    templateUrl: 'password-setting.component.html',
    styles: []
})
export class PasswordSettingComponent implements OnInit {
    public desktopSetting = [];
    public details = [];
    public role = '';
    public userRole = '';
    public desktopSettingId = 0;
    public desktopSettingDetail = [];
    public desktopUpdateStatus = false;
    itemListRole = [];
    createForm: FormGroup;
    createFormVersion: FormGroup;
    createFormSource: FormGroup;
    createFormBilling: FormGroup;
    createDesktopSettingForm: FormGroup;

    public urlId = 0;
    public profile: any;
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public id = 0;
    public moduleName = 'PasswordSettings';
    public booleans = {
        loader: false,
        loadingStatus: false,
        loadingAmount: false,
        loadingSource: false,
        loadingVersion: false,
        showSetting: true
    };

    public list = {
        allContent: [],
        roles: [],
        data: [],
        settingStatus: 'password'
    };


    static formData = function() {
        return {
            minimumLength: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
            maximumLength: ['', Validators.compose([Validators.required])],
            updatePeriod: ['', Validators.compose([Validators.required])],
            maxAllowedPassword: ['', Validators.compose([Validators.required])],
        }
    };

    static formDataSource = function() {
        return {
            source: ['', Validators.compose([Validators.required])],
        }
    };

    static formDataVersion = function() {
        return {
            version: ['', Validators.compose([Validators.required])],
        }
    };

    static formDataBill = function() {
        return {
            amount: ['', Validators.compose([Validators.required])],
        }
    };

    static desktopSettingData = function() {
        return {
            name: ['', Validators.compose([Validators.required])],
            value: ['', Validators.compose([Validators.required])],
        }
    };

    settingsValues = {
        minimumLength: null,
        maximumLength: null,
        updatePeriod: null,
        maxAllowedPassword: null,
    }

    constructor(private router: Router,
        private passwordSettings: PasswordService,
        private fb: FormBuilder,
        private eventService: EventsService,
        private notification: NotificationService) {
        this.createForm = this.fb.group(PasswordSettingComponent.formData());
        this.createFormBilling = this.fb.group(PasswordSettingComponent.formDataBill());
        this.createFormSource = this.fb.group(PasswordSettingComponent.formDataSource());
        this.createFormVersion = this.fb.group(PasswordSettingComponent.formDataVersion());
        this.createDesktopSettingForm = this.fb.group(PasswordSettingComponent.desktopSettingData());
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        const getRole = Cache.get('credentials')['user']['role']['name'];
        if (getRole && getRole !== '') {
            this.userRole = getRole.toLowerCase();
        }
        this.onGetPasswordSettings();
        this.getAmount();
        // this.getVersion();
        // this.getSource();
        this.getDesktopSetting();

    }

    ngAfterViewInit() {

        mLayout.initHeader();

    }

    onGetPasswordSettings() {
        this.booleans.loader = true;
        this.passwordSettings.getPasswordSettings()
            .subscribe((response) => {
                this.booleans.loader = false;
                this.settingsValues = response;
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                // this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    onSave() {
        this.booleans.loadingStatus = true;
        this.passwordSettings.updatePassword(this.createForm.value)
            .subscribe((response) => {
                this.booleans.loadingStatus = false;
                this.notification.success('Settings successfully updated');
            },
            error => {
                this.booleans.loadingStatus = false;
                this.notification.error('Unable to Update password', error);
            })
    }

    onSaveBillAmount() {
        this.booleans.loadingAmount = true;
        if (this.createFormBilling.value['amount'] < 0) {
            this.booleans.loadingAmount = false;
            return this.notification.error('Amount cannot be less than Zero');
        }
        this.passwordSettings.saveAmount(this.createFormBilling.value)
            .subscribe((response) => {
                this.booleans.loadingAmount = false;
                this.notification.success('Settings successfully updated');
            },
            error => {
                this.booleans.loadingAmount = false;
                this.notification.error('Unable to update amount', error);
            })
    }

    onSaveVersion() {
        this.booleans.loadingVersion = true;
        this.passwordSettings.saveVersion(this.createFormVersion.value)
            .subscribe((response) => {
                this.booleans.loadingVersion = false;
                this.notification.success('Settings successfully updated');
            },
            error => {
                this.booleans.loadingVersion = false;
                this.notification.error('Unable to update amount', error);
            })
    }

    onSaveSource() {
        this.booleans.loadingSource = true;
        this.passwordSettings.saveSource(this.createFormSource.value)
            .subscribe((response) => {
                this.booleans.loadingSource = false;
                this.notification.success('Settings successfully updated');
            },
            error => {
                this.booleans.loadingSource = false;
                this.notification.error('Unable to update amount', error);
            })
    }

    onSaveDesktopSetting() {
        this.booleans.loadingSource = true;
        this.passwordSettings
            .saveDesktopSetting({ value: this.createDesktopSettingForm.value }, this.desktopUpdateStatus, this.desktopSettingId)
            .subscribe((response) => {
                this.booleans.loadingSource = false;
                this.createDesktopSettingForm.reset();
                console.log('setting response :: ', response);
                if (this.desktopUpdateStatus) {
                    this.desktopSetting.forEach((value, i) => {
                        if (+value.id === +this.desktopSettingId) {
                            this.desktopSetting[i] = response
                        }
                    });
                } else {
                    this.desktopSetting.unshift(response);
                }
                $('#CreateModal').modal('hide');
                this.notification.success('Settings successfully updated');
            },
            error => {
                this.booleans.loadingSource = false;
                this.notification.error('Unable to update setting', error);
            })
    }

    getAmount() {
        this.passwordSettings.getAmount()
            .subscribe((response) => {
                response['amount'] = response['value'];
                this.createFormBilling = this.fb.group(response);
            },
            error => {
                this.notification.error('Unable to load amount', error);
            })
    }

    getVersion() {
        this.passwordSettings.getVersion()
            .subscribe((response) => {
                response['version'] = response['value'];
                this.createFormVersion = this.fb.group(response);
            },
            error => {
                this.notification.error('Unable to load amount', error);
            })
    }

    getSource() {
        this.passwordSettings.getSource()
            .subscribe((response) => {
                response['source'] = response['value'];
                this.createFormSource = this.fb.group(response);
            },
            error => {
                this.notification.error('Unable to load amount', error);
            })
    }

    getDesktopSetting() {
        this.passwordSettings.desktopSetting()
            .subscribe((response) => {
                this.desktopSetting = response;
            },
            error => {
                this.notification.error('Unable to load setting', error);
            })
    }

    preventNegative(type) {
        switch (type) {
            case 'max': {
                if (this.settingsValues.maximumLength < 0) {
                    this.settingsValues.maximumLength = 0;
                    return false;
                }
                break;
            }
            case 'min': {
                if (this.settingsValues.minimumLength < 0) {
                    this.settingsValues.minimumLength = 0;
                    return false;
                }
                break;
            }
            case 'period': {
                if (this.settingsValues.updatePeriod < 0) {
                    this.settingsValues.updatePeriod = 0;
                    return false;
                }
                break;
            }
            case 'maxAllow': {
                if (this.settingsValues.maxAllowedPassword < 0) {
                    this.settingsValues.maxAllowedPassword = 0;
                    return false;
                }
                break;
            }
        }
    }

    onSwitch(status) {
        this.list.settingStatus = status;
    }

    onCreateDesktopSetting() {
        this.createDesktopSettingForm.reset();
        this.desktopUpdateStatus = false;
        $('#CreateModal').modal('show');
    }

    getDesktopSettingDetails(data) {
        this.desktopUpdateStatus = true;
        $('#CreateModal').modal('show');
        this.desktopSettingId = data.id;
        this.createDesktopSettingForm = this.fb.group(data);
    }
}