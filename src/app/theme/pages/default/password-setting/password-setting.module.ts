import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PasswordSettingComponent } from './password-setting.component';
import { RouterModule, Routes } from "@angular/router";
import { DefaultComponent } from "../default.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { LayoutModule } from "../../../layouts/layout.module";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": PasswordSettingComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        AngularMultiSelectModule,
        NgbModule,
        PipeModule,

    ],
    declarations: [PasswordSettingComponent]
})
export class PasswordSettingModule { }
