import { Component, OnInit } from '@angular/core';
import { Cache } from "../../../../../utils/cache";

@Component({
    selector: 'app-audit-report',
    templateUrl: 'audit-report.component.html',
    styleUrls: ['audit-report.component.css']
})
export class AuditReportComponent implements OnInit {
    public status = false;
    public role = '';
    public webCount: any;
    public desktopCount: any;

    constructor() {
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        if (this.role === 'NIBSS') {
            this.webCount = Cache.get('totalWebAudit');
            this.desktopCount = Cache.get('totalDesktopAudit');
        }
    }

    onSwitch(bools) {
        this.status = bools;
    }
}