import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditReportComponent } from './audit-report.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { LayoutModule } from "../../../layouts/layout.module";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { DefaultComponent } from "../default.component";
import { StandaloneAuditComponent } from './standalone-audit/standalone-audit.component';
import { WebAuditComponent } from './web-audit/web-audit.component';
import { AmChartsModule } from "@amcharts/amcharts3-angular";

const RouterRoute: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            { path: '', component: AuditReportComponent },
            /*  { path: 'desktop', component: StandaloneAuditComponent },
              { path: 'web', component: WebAuditComponent },*/
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(RouterRoute),
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        LayoutModule,
        AngularMultiSelectModule,
        NgbModule,
        AmChartsModule,
    ],
    declarations: [AuditReportComponent, StandaloneAuditComponent, WebAuditComponent]
})
export class AuditModule { }
