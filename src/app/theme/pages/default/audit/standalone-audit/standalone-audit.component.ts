import { Component, OnInit } from '@angular/core';
import { NotificationService } from "../../../../../../services/notification.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuditService } from "../../../../../../services/api-service/audit.service";
import { Cache } from "../../../../../../utils/cache";

@Component({
    selector: 'app-standalone-audit',
    templateUrl: 'standalone-audit.component.html',
    styles: []
})
export class StandaloneAuditComponent implements OnInit {
    searchOption = 'Search Type';
    public current: number = 1;
    public dateRangeSearchForm: FormGroup;


    public urlId = 0;
    // public searchOption = 'Search Type';
    public profile: any;
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public id = 0;
    public moduleName = 'Desktop Audit';
    public booleans = {
        loader: false,
        loadingStatus: false,
        loadsearch: false,
    };

    static searchDateRangeForm = () => {
        return {
            params: ['', Validators.compose([])],
            from: ['', Validators.compose([])],
            to: ['', Validators.compose([])],
        }
    };

    public list = {
        allContent: [],
        roles: [],
        data: [],
    };


    constructor(private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private auditService: AuditService,
        private route: ActivatedRoute) {
        this.dateRangeSearchForm = this.fb.group(StandaloneAuditComponent.searchDateRangeForm());
    }

    ngOnInit() {
        /*this.parentRoute = `For ${Cache.get('audit')}`;
        this.urlId = +this.route.snapshot.paramMap.get('id');*/
        this.loadFunction();
        /*this.profile = Cache.get('credentials');*/
    }

    onNavBack() {
        let previousUrl = Cache.get('previousUrl');
        this.router.navigate([previousUrl]);
    }


    loadFunction() {
        this.onGetAllDesktopAudit(this.p - 1);
    }


    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        this.onGetAllDesktopAudit(this.p - 1);
    }

    onGetAllDesktopAudit(pageNo) {
        this.booleans.loader = true;
        this.auditService.getDesktopAudit(pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                console.log('audit :: ', response);
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    onSearchType(searchType) {
        this.searchOption = searchType;
    }

    onSearch() {
        this.booleans.loadsearch = true
    }

    dateRangeSearchbyLog() {
        if (this.dateRangeSearchForm.value['params'] === '' && this.dateRangeSearchForm.value['from'] === '' && this.dateRangeSearchForm.value['to'] === '') {
            this.notification.error('Please, enter a value in the parameter');
        } else if ((this.dateRangeSearchForm.value['from'] !== '' && this.dateRangeSearchForm.value['to'] === '')
            || (this.dateRangeSearchForm.value['from'] === '' && this.dateRangeSearchForm.value['to'] !== '')) {
            this.notification.error('Please, enter a value in the parameter');
        } else {
            this.booleans.loadsearch = true;
            // this.searchParams = this.dateRangeSearchForm.value;
            this.auditService.getSearchLog(this.dateRangeSearchForm.value, 0)
                .subscribe((response) => {
                    console.log('response :: ', response)
                    this.booleans.loadsearch = false;
                    if (response.content.length === 0) {
                        this.notification.error('No Result Found');
                    } else {
                        this.list.allContent = response.content;
                    }
                    this.dateRangeSearchForm.reset();
                },
                error => {
                    console.log('error :: ', error);
                    this.booleans.loadsearch = false;
                    this.notification.error('Unable Search the log', error);
                })
        }
    }

}
