import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllEnrollerComponent } from "./all-enroller.component";
import { RouterModule, Routes } from "@angular/router";
import { DefaultComponent } from "../default.component";
import { NgxPaginationModule } from "ngx-pagination";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { LayoutModule } from "../../../layouts/layout.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

const RouterRoute: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            { path: '', component: AllEnrollerComponent }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(RouterRoute),
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        LayoutModule,
        FormsModule,
        AngularMultiSelectModule,
        NgbModule
    ],
    declarations: [
        AllEnrollerComponent
    ]
})
export class AllEnrollerModule {
}
