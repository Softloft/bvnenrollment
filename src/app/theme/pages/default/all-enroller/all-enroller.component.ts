import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { EnrollerService } from "../../../../../services/api-service/enroller.service";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationService } from "../../../../../services/notification.service";
import { StateService } from "../../../../../services/api-service/state.service";
import { ConfigService } from "../../../../../services/api-service/config.service";
import { BranchService } from "../../../../../services/api-service/branch.service";
import { Cache } from "../../../../../utils/cache";
import { AgentService } from "../../../../../services/api-service/agent.service";
import { DashboardService } from "../../../../../services/api-service/dashboard.service";

declare const $;
declare var swal: any;


@Component({
    selector: 'app-all-enroller',
    templateUrl: 'all-enroller.component.html',
    styles: []
})
export class AllEnrollerComponent implements OnInit {
    public p = 1;
    public download = '';
    public totalElement = 0;
    public toDate: any;
    public fromDate: any;
    public toDate1: any;
    public fromDate1: any;
    public search = {
        to: [],
        from: []
    };
    public id = 0;
    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false
    };

    public list = {
        allContent: [],
    };


    constructor(private branchService: BranchService,
        private dashboardservice: DashboardService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadFunction();
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.getSearchLog(this.p - 1);
        } else {
            this.getAllEnroller(this.p - 1);
        }
    }

    loadFunction() {
        this.getAllEnroller(this.p - 1);
    }

    onSelectDownType(type) {
        this.download = type;
    }

    getSearchLog(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.dashboardservice.getSearchLog(this.search, pageNo, 'enrollers')
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                console.log('response from search ', response);
                if (response.content.length === 0) {
                    this.booleans.searchStatus = false;
                    this.notification.error('No result Found');
                } else {
                    // console.log('all :: ', response)
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })
    }

    getAllEnroller(pageNo) {
        this.booleans.searchStatus = false;
        this.dashboardservice.getAllEnroller(pageNo)
            .subscribe((response) => {
                console.log('all enroller :: ', response)
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                console.log('errors :: ', error)
                this.notification.error('Unable to load Branch', error);
            })
    }

    onOpen() {
        $('#CreateModal').modal('show');
    }

    exportDownload() {
        this.search['to'] = this.toDate1;
        this.search['from'] = this.fromDate1;
        this.dashboardservice.exportdownload(this.search, this.download, 'enrollers')
        /*.subscribe((response) => {
                this.booleans.loadingSearch = false;
                window.open(response, "_blank");
                console.log('response from export222 ', response);
                /!*if(response.content.length === 0){
                    this.booleans.searchStatus= false;
                    this.notification.error('No result Found');
                }else{
                    // console.log('all :: ', response)
                    this.list.allContent = response.content;
                }*!/
            },
            error => {
                window.open(error, "_blank");
                console.log('error download :: ', error)
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })*/
    }
}
