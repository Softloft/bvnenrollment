import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnrollerComponent } from './enroller.component';
import { BranchComponent } from "../branch/branch.component";
import { DefaultComponent } from "../default.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { LayoutModule } from "../../../layouts/layout.module";
import { RouterModule, Routes } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { PipeModule } from "../../../../../shared/modules/pipe.module";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": EnrollerComponent,
            },
            {
                "path": ":id",
                "component": EnrollerComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        AngularMultiSelectModule,
        NgbModule,
        PipeModule
    ],
    declarations: [EnrollerComponent]
})
export class EnrollerModule {
}
