import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BranchComponent } from './branch.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DefaultComponent } from "../default.component";
import { ActivatedRoute, RouterModule, Routes } from "@angular/router";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { LayoutModule } from "../../../layouts/layout.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": BranchComponent,
            },
            {
                "path": ":id",
                "component": BranchComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        FormsModule,
        AngularMultiSelectModule
    ],
    declarations: [BranchComponent]
})
export class BranchModule {
}
