import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { BranchService } from "../../../../../services/api-service/branch.service";
import { StateService } from "../../../../../services/api-service/state.service";
import { ConfigService } from "../../../../../services/api-service/config.service";
import { NotificationService } from "../../../../../services/notification.service";
import { Route, ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../utils/cache";
import { SearcherService } from "../../../../../services/searcher.service";

declare const $;

@Component({
    selector: 'app-branch',
    templateUrl: './branch.component.html',
    styles: []
})
export class BranchComponent implements OnInit {
    public lgaId = null;
    public searchOption = 'Search Options';
    public searchtext = '';

    itemListState = [];
    selectedItemsState = [];
    settingsState = {};

    itemListLga = [];
    selectedItemsLga = [];
    settingsLga = {};

    public urlId = 0;
    public role = '';
    public userId = 0;
    public checkAgent = '';
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public id = 0;
    public agentId = 0;
    public moduleName = 'Branch';
    public createForm: FormGroup;
    public booleans = {
        loadingSearch: false,
        searchStatus: false,
        showUpdate: false,
        loader: false,
        loadingStatus: false,
        loadRole: false,
        stateStatus: false,
        lgaStatus: false,
        cityStatus: false,
        emptyState: false,
        emptyLga: false,
        emptyCity: false,
    };

    public list = {
        allContent: [],
        roles: [],
        tasks: [],
        data: [],
        allState: [],
        allLga: [],
        allCity: [],
    };

    static formdata = function() {
        return {
            name: ['', Validators.compose([Validators.required])],
            contactDetails: new FormGroup({
                streetNumber: new FormControl('', Validators.required),
                city: new FormControl('', Validators.required),
                lgaId: new FormControl('', Validators.required),
                state: new FormControl('', Validators.required)
            }),
        }
    };

    constructor(private branchService: BranchService,
        private configservice: ConfigService,
        private searchservice: SearcherService,
        private stateService: StateService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
        this.createForm = this.fb.group(BranchComponent.formdata());
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        if (this.role === 'AGENT') {
            this.list.tasks = Cache.get('tasks');
            this.userId = Cache.get('credentials')['id'];
            // this.urlId = +this.route.snapshot.paramMap.get('id');
            this.parentRoute = `For ${Cache.get('agent')}`;
            this.urlId = Cache.get('credentials')['id'];
            this.loadFunction();

        }
    }

    onNavBack() {
        this.router.navigate(['/agent', +Cache.get('agentId')]);
    }

    onNavAll() {
        this.router.navigate(['/agent/all']);
    }

    loadFunction() {
        this.onGetAllBranch(this.p - 1);
        this.getState();
    }


    onGetActivatedRole() {
        this.booleans.loadRole = true;
        this.configservice.getActivatedRole('AGENT')
            .subscribe((response) => {
                this.list.roles = response.content;
                this.booleans.loadRole = false;
            },
            error => {
                this.booleans.loadRole = false;
                this.notification.error('Unable to load Role', error);
            })
    }

    onGetAllBranch(pageNo) {
        /*if (this.urlId === 0) {
            this.urlId = this.userId;
            this.parentRoute = '';
        }*/
        this.booleans.loader = true;
        this.branchService.getAllBranches(this.urlId, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                console.log('branch :: ', response);
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    onGetFullBranchList() {
        this.booleans.loader = true;
        this.branchService.getbranchesFull()
            .subscribe(
            (response) => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            }
            )
    }

    insertForm(action, data?: any) {
        if (this.createForm.value['contactDetails']['lgaId']) {
            this.createForm.value['contactDetails']['lgaId'] = this.createForm.value['contactDetails']['lgaId'][0]['id'];
            this.createForm.value['contactDetails']['state'] = this.createForm.value['contactDetails']['state'][0]['id'];
        }
        this.createForm.value['agentId'] = this.urlId;
        if (action === 'update') {
            this.list.data = this.createForm.value;
            this.post(action);
        } else if (action === 'toggle') {
            this.list.data = data;
            this.post(action);
        } else if (action === 'new') {
            this.list.data = this.createForm.value;
            this.post(action);
        }

    }

    private post(action) {
        this.booleans.loadingStatus = true;
        this.branchService.insertForm(this.urlId, this.list.data, action)
            .subscribe((response) => {
                console.log('log ::', response);
                this.booleans.loadingStatus = false;
                if (action === 'update' || action === 'toggle') {
                    this.list.allContent.forEach((val, i) => {
                        if (val.id === this.list.data['id']) {
                            this.list.allContent[i] = response;
                        }
                    });
                    this.notification.success('Update was successful');
                } else {
                    this.list.allContent.unshift(response);
                }
                $('#CreateModal').modal('hide');
            },
            error => {
                this.booleans.loadingStatus = false;
                this.notification.error(`Unable to ${action} ${this.moduleName}`, error)
            });
    }


    onOpen() {
        this.list.allLga = [];
        this.booleans.showUpdate = false;
        this.createForm.reset();
        this.booleans.emptyCity = false;
        this.booleans.emptyLga = false;
        this.booleans.emptyState = false;
        this.booleans.showUpdate = false;
        $('#CreateModal').modal('show');
    }

    onEdit(data) {
        this.lgaId = [{ 'id': data.contactDetails.lga.id, 'itemName': data.contactDetails.lga.name }];
        this.getLga(data.contactDetails.lga.state.id);
        data['contactDetails']['streetNumber'] = data.contactDetails.streetNumber;
        data['contactDetails']['city'] = data.contactDetails.city;
        const formdata = function() {
            return {
                agentId: [data.agent.id, Validators.compose([Validators.required])],
                id: [data.id, Validators.compose([Validators.required])],
                name: [data.name, Validators.compose([Validators.required])],
                contactDetails: new FormGroup({
                    streetNumber: new FormControl(data.contactDetails.streetNumber, Validators.required),
                    city: new FormControl(data.contactDetails.city, Validators.required),
                    lgaId: new FormControl([{
                        'id': data.contactDetails.lga.id,
                        'itemName': data.contactDetails.lga.name
                    }], Validators.required),
                    state: new FormControl([{
                        'id': data.contactDetails.lga.state.id,
                        'itemName': data.contactDetails.lga.state.name
                    }], Validators.required)
                }),
            }
        }
        this.createForm = this.fb.group(formdata());
        this.booleans.showUpdate = true;
        this.booleans.emptyCity = false;
        this.booleans.emptyLga = false;
        this.booleans.emptyState = false;
        $('#CreateModal').modal('show');
    }

    onView(data) {
        console.log('data :: ', data);
        this.list.data = data;
        $('#viewModal_1').modal('show');
    }

    getState() {
        this.booleans.stateStatus = true;
        console.log('get state')
        this.stateService.getState().subscribe(
            (response) => {
                this.list.allState = response;
                this.list.allState.forEach((value) => {
                    this.itemListState.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.fetchState();

                console.log('the State::', response);

                if (this.list.allState.length === 0) {
                    this.booleans.emptyState = true;
                }
                this.booleans.stateStatus = false;
            },
            (error) => {
                this.booleans.stateStatus = false;
                this.notification.error('Unable load states ', error);
            }
        )
    }

    getLga(id) {
        this.booleans.lgaStatus = true;
        this.stateService.getLga(id).subscribe(
            (response) => {
                this.list.allLga = response;
                console.log('all lga :: ', this.list.allLga)
                this.list.allLga.forEach((value) => {
                    this.itemListLga.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.fetchLga();
                if (this.list.allLga.length === 0) {
                    this.booleans.emptyLga = true;
                }
                this.booleans.lgaStatus = false;
            },
            (error) => {
                this.booleans.lgaStatus = false;
                this.notification.error('Unable load Lga ', error);
            }
        )
    }

    getCity(event) {
        const id = event.target.value;
        this.booleans.cityStatus = true;
        this.stateService.getCity(id).subscribe(
            (response) => {
                this.list.allCity = response;
                if (this.list.allCity.length === 0) {
                    this.booleans.emptyCity = true;
                }
                this.booleans.cityStatus = false;
            },
            (error) => {
                this.booleans.cityStatus = false;
                this.notification.error('Unable load City ', error);
            }
        )
    }

    onViewAgent(id) {
        this.router.navigate(['/agent/branch', id]);
    }

    fetchLga() {
        this.settingsLga = {
            singleSelection: true,
            text: "Select Lga",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }

    onItemSelectLga(item: any) {
        this.selectedItemsLga = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectLga(item: any) {
    }

    onSelectAllLga(items: any) {
    }

    onDeSelectAllLga(items: any) {
    }

    fetchState() {
        this.settingsState = {
            singleSelection: true,
            text: "Select State",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }

    onItemSelectState(item: any) {
        this.list.allLga = [];
        this.itemListLga = [];
        this.lgaId = null;
        this.selectedItemsState = [{ 'id': item.id, 'itemName': item.itemName }];
        this.getLga(item.id);
    }

    OnItemDeSelectState(item: any) {
    }

    onSelectAllState(items: any) {
    }

    onDeSelectAllState(items: any) {
    }

    onSearchType(searchType) {
        this.searchOption = searchType;
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.onGetAllBranch(this.p - 1);
        } else {
            this.searchMethod(this.p - 1);
        }
    }


    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.searchservice.databaseSearch(this.searchtext, 'branch', pageNo)
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }
}
