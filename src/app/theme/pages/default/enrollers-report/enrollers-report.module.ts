import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnrollersReportComponent } from './enrollers-report.component';
import { DefaultComponent } from "../default.component";
import { ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { RouterModule, Routes } from "@angular/router";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { LayoutModule } from "../../../layouts/layout.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": EnrollersReportComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule
    ],
    declarations: [EnrollersReportComponent]
})
export class EnrollersReportModule { }
