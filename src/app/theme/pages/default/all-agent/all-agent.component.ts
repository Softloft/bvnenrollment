import { Component, ElementRef, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { AgentService } from "../../../../../services/api-service/agent.service";
import { ConfigService } from "../../../../../services/api-service/config.service";
import { StateService } from "../../../../../services/api-service/state.service";
import { NotificationService } from "../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CopingsService } from "../../../../../services/copings.service";
import { Cache } from "../../../../../utils/cache";
import { SearcherService } from "../../../../../services/searcher.service";
import { InstitutionService } from "../../../../../services/api-service/institution.service";
import { saveAs } from 'file-saver';
import { FileUploader } from "ng2-file-upload";
import { environment } from "../../../../../environments/environment";
import { defaultHostName } from "../../../../../utils/check-hostname";


declare const $;
declare var swal: any;

@Component({
    selector: 'app-all-agent',
    templateUrl: 'all-agent.component.html',
    styleUrls: ['all-agent.component.css']
})
export class AllAgentComponent implements OnInit {
    public id = 0;
    public role = '';
    public userRole = '';
    public searchOption = 'Search Options';
    public searchtext = '';
    public uploader: FileUploader = new FileUploader({ url: `${defaultHostName()}${environment.API_URL.bulk}/agent` });

    public userType = '';
    public submitFile = false;
    public urlId = '';
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public fileName = '';
    public profile = '';
    public moduleName = 'All Agent';
    public createForm: FormGroup;
    public downloadedFile = [];
    public booleans = {
        loadingSearch: false,
        searchStatus: false,
        loadingInstitution: false,
        showUpdate: false,
        loader: false,
        loadingStatus: false,
        loadingAgtMgr: false,
    };
    public list = {
        user: '',
        allContent: [],
        data: [],
        institution: [],
        agentMgr: []
    };
    static formdata = function() {
        return {
            newAgentManagerId: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private agentService: AgentService,
        private institutionservice: InstitutionService,
        private stateService: StateService,
        private searchservice: SearcherService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private elem: ElementRef,
        private copings: CopingsService,
        private route: ActivatedRoute) {
        this.createForm = this.fb.group(AllAgentComponent.formdata());
    }


    ngOnInit() {
        this.onGetInstitution();
        this.userType = Cache.get('credentials')['makerCheckerType'];
        this.role = Cache.get('credentials')['user']['userType'];
        const getRole = Cache.get('credentials')['user']['role']['name'];
        if (getRole && getRole !== '') {
            this.userRole = getRole.toLowerCase();
        }
        if (this.role === 'NIBSS') {
            this.parentRoute = `For ${Cache.get('agentMgr')}`;
            Cache.set('previousUrl', this.router.url);
            this.urlId = 'all';
            Cache.set('agentId', this.urlId);
            this.onGetAllAgent(this.p - 1);
            this.profile = Cache.get('credentials');
        }
    }


    onNavBack() {
        this.router.navigate(['/agent']);
    }

    onGetAllAgent(pageNo) {
        this.booleans.loader = true;
        this.booleans.searchStatus = false;
        this.agentService.getAllAgent(pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.list.user = response;
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    onGetAgtMgr(event) {
        const id = +event.target.value;
        this.booleans.loadingAgtMgr = false;
        this.agentService.getAgentbyInstitutionId(id)
            .subscribe((response) => {
                this.booleans.loadingAgtMgr = true;
                this.list.agentMgr = response;
            },
            error => {
                this.booleans.loadingAgtMgr = true;
                this.notification.error('Unable to load Agent Manager', error);
            });
    }


    onGetInstitution() {
        this.booleans.loadingInstitution = false;
        this.institutionservice.getInstitutionList()
            .subscribe((response) => {
                this.list.institution = response;
                this.booleans.loadingInstitution = true;
            },
            error => {
                this.booleans.loadingInstitution = true;
                this.notification.error('Unable to load Institution', error);
            })
    }


    public post() {
        this.createForm.value['agentId'] = this.id;
        this.booleans.loadingStatus = true;
        this.agentService.switch(this.createForm.value)
            .subscribe((response) => {
                console.log('all switch ::', response)
                this.booleans.loadingStatus = false;
                this.list.allContent.forEach((val, i) => {
                    if (val.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                this.notification.success('Switch was successful');
                $('#CreateModal').modal('hide');
            },
            error => {
                this.booleans.loadingStatus = false;
                this.notification.error(`Unable to Switch Agent`, error)
            });
    }

    /**
     * getting all the bulk file
     */
    getBulkFile() {
        this.agentService.getBulkFile()
            .subscribe(response => {
                window.open(`${response}?vim=${Cache.get('token')}`, "_blank");
            },
            err => {
                this.notification.error('Unable to download template', err);
            }
            );
    }

    createBulkFile() {
        this.agentService.getBulkFile();
    }

    onSelectFile() {
        const file = this.elem.nativeElement.querySelector('#customFile');
        const $linkId = $('#customFile');
        this.fileName = $linkId.val();
    }

    public submitBulkFile() {
        const file = this.elem.nativeElement.querySelector('#customFile');
        const $linkId = $('#customFile');
        const $newLinkId = $linkId.val();
        const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
        if (file_type !== '.xls') {
            $linkId.addClass('alert alert-danger animated rubberBand');
            return this.notification.error('You can only upload xls files');
        }
        this.submitFile = true
        this.agentService.createDeptBulk(file).subscribe(
            (response) => {
                this.fileName = '';
                this.submitFile = false;
                saveAs(response, 'template');
                this.notification.success('Bulk upload was Successful');
            },
            (err) => {
                this.submitFile = false;
                this.notification.error('there was an error while uploading, please retry', err);
            }
        );
    }


    onOpen(data) {
        this.id = data.id;
        this.createForm.reset();
        $('#CreateModal').modal('show');
    }


    onView(data) {
        this.list.data = data;
        $('#viewModal2').modal('show');
    }


    onSearchType(searchType) {
        this.searchOption = searchType;
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.searchMethod(this.p - 1);
        } else {
            this.onGetAllAgent(this.p - 1);
        }
    }


    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.searchservice.databaseSearch(this.searchtext, 'agent', pageNo)
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }


}