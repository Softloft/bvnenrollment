import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule, Routes } from "@angular/router";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgPureDatatableModule } from "ng-pure-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { DefaultComponent } from "../default.component";
import { AllAgentComponent } from "./all-agent.component";
import { LayoutModule } from "../../../layouts/layout.module";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { AgentManagersComponent } from "../agent-managers/agent-managers.component";
import { AgentComponent } from "../agent-managers/agent/agent.component";
import { FileUploadModule } from "ng2-file-upload";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": AllAgentComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        NgPureDatatableModule,
        FileUploadModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        PipeModule,
        AngularMultiSelectModule,
        FormsModule
    ],
    declarations: [AllAgentComponent]
})
export class AllAgentModule { }
