import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { Cache } from "../../../../../utils/cache";

declare const $;
@Component({
    selector: "app-profile",
    templateUrl: "./profile.component.html",
    styleUrls: ['./profile.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit {
    public selectedModule;
    public showDrop = false;
    public details = [];
    public list = {
        moduleName: [],
        moduleTasks: [],
        alltasks: [],
        task: []
    };
    public role = [];
    public tasks = {
        module: '',
        task: [],
        mapping: {
            name: '',
            description: '',
            url: ''
        },

    };

    constructor() {
        this.checkAutoModalTrigger();
    }

    ngOnInit() {
        this.details = Cache.get('credentials');
        console.log('details :: ', this.details)
        this.list.alltasks = this.details['user']['role']['tasks'];
        this.list.alltasks.forEach((value) => {
            if (this.list.moduleName.indexOf(value['module']) === -1) {
                this.list.moduleName.push(value['module']);
            }
        });
    }

    /**
     *  on selecting the module name when assigning a task,
     *  the selected module name is used get its tasks
     *  which is populated on the checkbox option
     * selected task on creating group
     * @param moduleName
     */
    selectTask(moduleName) {
        if (this.list.task === moduleName && this.showDrop) {
            this.showDrop = false;
        } else {
            this.showDrop = true;
        }
        this.list.moduleTasks = [];
        this.list.task = moduleName;
        this.list.alltasks.forEach((value) => {
            if (value['module'] === moduleName) {
                this.list.moduleTasks.push(value);
            }
        });
        // console.log('loggin :: ', this.list.moduleTasks);
        this.selectedModule = `#collapseOne${moduleName}`;
    }
    public openPasswordModal() {
        $('#passwordModal').modal({ show: true, backdrop: 'static', keyboard: false });
    }
    public checkAutoModalTrigger() {
        const userData = Cache.get('credentials');
        (userData['user']['change_password']) ? setTimeout(() => { this.openPasswordModal(); }, 2000) : console.log('Hekllo world Modal fails');
    }
}