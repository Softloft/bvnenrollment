import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { PasswordService } from "../../../../../../services/password.service";
import { NotificationService } from "../../../../../../services/notification.service";
import { Cache } from "../../../../../../utils/cache";
import { Router } from "@angular/router";
import Swal from 'sweetalert2'
import { UserService } from "../../../../../../services/user.service";

declare const $: any;

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styles: []
})
export class ChangePasswordComponent implements OnInit {
    changePasswordForm: FormGroup;
    changePwd = {
        oldPassword: null,
        newPassword: null,
        reNewPassword: null
    };
    booleans = {
        loadingStatus: false,
        rePassword: false,
        enableBtnSubmit: true
    };
    username: string;
    minLength: number;

    constructor(private formBuilder: FormBuilder,
        public passwordService: PasswordService,
        public userservice: UserService,
        private notificationService: NotificationService,
        private router: Router) {
        this.minLength = 8;
        this.getPasswordPolicy();
    }

    ngOnInit() {
        this.initializeForm();
        const userObject = Cache.get('credentials')
        console.log('username :: ', userObject['user']['username']);
        this.username = userObject['user']['username'];
        // this.username = userObject['user']['username']
    }

    /**
     * method to build the reactive form.
     */
    public initializeForm() {
        this.changePasswordForm = this.formBuilder.group({
            oldPassword: ['', Validators.compose([Validators.required, Validators.minLength(this.minLength)])],
            newPassword: ['', Validators.compose([Validators.required, Validators.minLength(this.minLength)])],
            reNewPassword: ['', Validators.compose([Validators.required])]
        });
    }

    /**
     * Fire when user submit to change password
     */
    public changeUserPassword() {
        // console.log('Changing password');
        let middlename
        const details = Cache.get('credentials');
        const passwordObject = {
            password: this.changePwd.oldPassword,
            newPassword: this.changePwd.newPassword,
            username: this.username
        };
        console.log('details password object :: ', passwordObject);
        this.booleans.enableBtnSubmit = false;
        const username = details['user']['username'].toLowerCase();
        const firstName = details['user']['name']['firstName'].toLowerCase();
        const lastName = details['user']['name']['lastName'].toLowerCase();
        if (details['user']['name']['middleName']) {
            middlename = details['user']['name']['middleName'].toLowerCase();
        }
        console.log('middlename :: ', middlename);
        console.log('username :: ', username);
        const newPassword = this.changePasswordForm.value['newPassword'].toLowerCase();
        if ((newPassword.search(username) > -1)) {
            this.notificationService.error('Your username must not be included in the password');
        } else if ((newPassword.search(firstName) > -1) || (newPassword.search(lastName) > -1) || (middlename && newPassword.search(middlename) > -1)) {
            this.notificationService.error('Your firstname, middlename or lastname must not be included in the password');
        } else if ((newPassword.search('password') > -1) || (newPassword.search('admin') > -1) || (newPassword.search('user') > -1)) {
            this.notificationService.error('special word like password, user or admin must not be included in the password')
        } else {
            this.passwordService.changePassword(passwordObject).subscribe((response) => {
                $('#passwordModal').modal({ show: false });
                $('#passwordModal').modal('hide');
                if (this.updateUserObj()) {
                    setTimeout(() => {
                        this.router.navigate(['/dashboard']);
                    }, 250);
                }
                this.notificationService.success(response.responseMessage);
                const passwordStatus = Cache.get('credentials')['user']['change_password'];
                if (passwordStatus) {
                    this.userservice.logout();
                    Swal('Password changed successfully', 'Login Again to Continue!', 'success')
                }

                this.booleans.enableBtnSubmit = true;
                this.changePasswordForm.reset();
            }, (error) => {
                // console.log('Error: ', error)
                this.notificationService.errorCustom('Error', error);
                this.booleans.enableBtnSubmit = true;
            })
        }

        this.booleans.enableBtnSubmit = true;
    }

    public updateRePassword() {
        this.booleans['rePassword'] = this.passwordService.strength >= 5;
    }

    public clearData() {
        this.changePasswordForm.reset();
    }

    public getPasswordPolicy() {
        this.passwordService.passwordPolicy().subscribe((response) => {
            // console.log('Response: ', response)
            this.minLength = response.minimumLength | 8;
        }, (error) => {
            // console.log('Error: ', error)
        })
    }

    async updateUserObj() {
        const userDetails = await Cache.get('bvn-auth-token');
        userDetails['userDetail'].user.change_password = false;
        // console.log('UserDetails: ', userDetails);
        await Cache.set('bvn-auth-token', userDetails);
        return true;
    }

}
