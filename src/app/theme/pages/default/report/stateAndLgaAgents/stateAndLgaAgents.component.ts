import { Component, OnInit } from '@angular/core';
import { MemberSyncService } from "../../../../../../services/member-sync.service";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationService } from "../../../../../../services/notification.service";

@Component({
    selector: 'app-state-and-lga-status',
    templateUrl: './stateAndLgaAgents.component.html',
    styleUrls: ['./stateAndLgaAgents.component.css']
})
export class StateAndLgaAgentsComponent implements OnInit {
    fromDate: any;
    toDate: any;
    reportSync: any;
    fromDate_: any;
    toDate_: any;
    loader: any;
    status = true;

    constructor(private memberSync: MemberSyncService,
        private activatedRoute: ActivatedRoute,
        private notification: NotificationService,
        private router: Router) {

    }

    ngOnInit() { }


    setDateRequired() {
        this.fromDate_ = `${this.fromDate['year']}-${this.fromDate['month']}-${this.fromDate['day']}`;
        this.toDate_ = `${this.toDate['year']}-${this.toDate['month']}-${this.toDate['day']}`;
    }

    reject() {
        return false;
    }


    preparePath() {
        sessionStorage.setItem('fromDate', this.fromDate_);
        sessionStorage.setItem('toDate', this.toDate_);
    }

    onSwitch(bools) {
        this.status = bools;
    }
}
