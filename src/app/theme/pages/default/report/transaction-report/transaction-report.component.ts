import { Component, ElementRef, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { NotificationService } from "../../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../../utils/cache";
import { saveAs } from "file-saver";
import { DashboardService } from "../../../../../../services/api-service/dashboard.service";
import { BranchService } from "../../../../../../services/api-service/branch.service";

declare const $;

@Component({
    selector: 'app-transaction-report',
    templateUrl: 'transaction-report.component.html',
    styleUrls: ['transaction-report.component.css']
})
export class TransactionReportComponent implements OnInit {
    transactionReportdetails = [];

    public modalType = '';
    public p = 1;
    public param = '';

    public userType = '';
    public fileName = '';
    public submitFile = false;

    public download = '';
    public totalElement = 0;
    public toDate1: any;
    public serviceProvided = '';
    public profile = [];
    public searchOption = 'Date';
    public enrollment = [];
    public toDate: any;
    public fromDate: any;
    public fromDate1: any;
    public institutionCode = '';
    itemList = [
        { "id": 'CASH_IN', "itemName": "Cash In", "category": "Banking Services" },
        { "id": 'CASH_OUT', "itemName": "Cash Out", "category": "Banking Services" },
        { "id": 'ACCOUNT_OPENING', "itemName": "Account Opening", "category": "Banking Services" },
        { "id": 'BILLS_PAYMENT', "itemName": "Bills Payment", "category": "Banking Services" },
        { "id": 'AIRTIME_RECHARGE', "itemName": "Airtime Recharge", "category": "Banking Services" },
        { "id": 'FUNDS_TRANSFER', "itemName": "Fund Transfer", "category": "Banking Services" },
        { "id": 'BVN_ENROLMENT', "itemName": "BVN Enrollment", "category": "Banking Services" },
        { "id": 'OTHERS', "itemName": "Others", "category": "Industry Services" },
        { "id": 'ADDITIONAL_SERVICE_1', "itemName": "Additional Services 1", "category": "Industry Services" },
        { "id": 'ADDITIONAL_SERVICE_2', "itemName": "Additional Services 2", "category": "Industry Services" },
    ];
    public search = {
        to: [],
        from: [],
        serviceProvided: '',
        institutionCode: '',
        param: ''
    };
    public id = 0;
    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false
    };

    public list = {
        allContent: [],
        data: [],
    };


    constructor(private branchService: BranchService,
        private dashboardservice: DashboardService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private elem: ElementRef,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadFunction();
        this.profile = Cache.get('credentials');
        this.enrollment = Cache.get('details');
        this.userType = Cache.get('credentials')['makerCheckerType'];
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.getSearchLog(this.p - 1);
        } else {
            this.getAllReport(this.p - 1);
        }
    }

    loadFunction() {
        this.getAllReport(this.p - 1);
        this.getDashboardTransactionReport();
    }

    getDashboardTransactionReport() {
        this.booleans.loader = true;
        this.dashboardservice.getDashboardtransactionreport()
            .subscribe((response) => {
                this.transactionReportdetails = response;
            },
            error => {
                this.notification.error('Unable to load Dashboard', error);
            })
    }
    onSelectDownType(type) {
        this.download = type;
    }

    onExport() {
        this.modalType = 'export'
        this.onOpen();
    }

    onModalSearchType() {
        this.modalType = 'search'
        this.onOpen();
    }

    getSearchLog(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.search['param'] = this.param;
        this.search['serviceProvided'] = this.serviceProvided;
        this.search['institutioncode'] = this.institutionCode;
        this.dashboardservice.searchReport(this.search, pageNo)
            .subscribe((response) => {
                $('#CreateModal').modal('hide');
                if (response.content.length === 0) {
                    this.booleans.searchStatus = false;
                    this.notification.error('No result Found');
                } else {
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })
    }

    getAllReport(pageNo) {
        this.p = pageNo + 1;
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.dashboardservice.getAllTransactionReport(pageNo)
            .subscribe((response) => {
                this.list.allContent = response.content;
                this.modalType = '';
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                this.modalType = '';
                this.booleans.loader = false;
                this.notification.error('Unable to load Enrollment', error);
            })
    }


    exportDownload() {
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.search['param'] = this.param;
        this.search['serviceProvided'] = this.serviceProvided;
        this.search['institutionCode'] = this.institutionCode;
        this.dashboardservice.exportReport(this.search)
        /*.subscribe((response) => {
                this.booleans.loadingSearch = false;
                //  window.open(response, "_blank");
                if(response.content.length === 0){
                    this.booleans.searchStatus= false;
                    this.notification.error('No result Found');
                }else{
                    this.list.allContent = response.content;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })*/
    }

    onOpen() {
        this.toDate = '';
        this.fromDate = '';
        this.serviceProvided = '';
        this.institutionCode = '';
        this.booleans.loadingSearch = false;

        $('#CreateModal').modal('show');
    }

    onSearch(type, count) {

    }

    onOpenModal(data) {
        this.list.data = data;
        $('#viewModal_12').modal('show');
    }

    onSearchType(type) {
        this.fromDate = null;
        this.toDate = null;
        this.param = '';
        this.searchOption = type;
    }


    onSelectFile() {
        const file = this.elem.nativeElement.querySelector('#customFile');
        const $linkId = $('#customFile');
        this.fileName = $linkId.val();
    }

    public submitBulkFile() {
        const file = this.elem.nativeElement.querySelector('#customFile');
        const $linkId = $('#customFile');
        const $newLinkId = $linkId.val();
        const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
        if (file_type !== '.xls') {
            $linkId.addClass('alert alert-danger animated rubberBand');
            return this.notification.error('You can only upload xls files');
        }
        this.submitFile = true
        this.dashboardservice.createDeptBulk(file).subscribe(
            (response) => {
                this.fileName = '';
                this.submitFile = false;
                saveAs(response, 'template');
                this.notification.success('Bulk upload was Successful');
            },
            (err) => {
                this.submitFile = false;
                this.notification.error('there was an error while uploading, please retry', err);
            }
        );
    }

    /**
     * getting all the bulk file
     */
    getBulkFile() {
        this.dashboardservice.getBulkFile()
            .subscribe(response => {
                window.open(`${response}?vim=${Cache.get('token')}`, "_blank");
            },
            err => {
                this.notification.error('Unable to download template', err);
            }
            );
    }
}
