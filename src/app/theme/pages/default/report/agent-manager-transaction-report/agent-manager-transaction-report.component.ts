import { Component, OnInit } from '@angular/core';
import { NotificationService } from "../../../../../../services/notification.service";
import { MemberSyncService } from "../../../../../../services/member-sync.service";
import { Router } from "@angular/router";
import { SearcherService } from "../../../../../../services/searcher.service";
import { Cache } from "../../../../../../utils/cache";
import { AgentService } from "../../../../../../services/api-service/agent.service";

declare const $;
@Component({
    selector: 'app-agent-manager-transaction-report',
    templateUrl: './agent-manager-transaction-report.component.html',
    styles: []
})
export class AgentManagerTransactionReportComponent implements OnInit {
    public searchOption = 'Date';
    public searchtext = '';
    public activeStatus = true;
    public urlId = 0;
    public toDate: any;
    public fromDate: any;

    public role = '';
    public p = 1;
    public totalElement = 0;
    public booleans = {
        loadingSearch: false,
        searchStatus: false,
        loader: false,
    };

    navDetails = {
        code: '',
        to: this.toDate,
        from: this.fromDate,
        param: this.searchtext
    };

    public list = {
        allContent: [],
        data: [],
        roles: [],
        tasks: [],
    };

    constructor(private notification: NotificationService,
        private syncService: MemberSyncService,
        private router: Router,
        private agentservice: AgentService) {
    }

    ngOnInit() {
        this.role = Cache.get('credentials')['user']['userType'];
        if (this.role === 'NIBSS') {
            this.list.tasks = Cache.get('tasks');
            this.loadFunction();
        }
    }


    loadFunction() {
        this.getAgentMgrReport(this.p - 1);
    }


    getAgentMgrReport(pageNo) {
        console.log('agent manager report')
        this.booleans.loader = true;
        let searchData = {
            from: '',
            to: '',
            username: '',
        };
        this.agentservice.getAgentMgrReport(pageNo)
            .subscribe((response) => {
                console.log('active response :: ', response)
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Sync Report', error);
            });
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.getAgentMgrReport(this.p - 1);
        } else {
            this.searchMethod(this.p - 1);
        }
    }


    onSearchType(type) {
        this.searchtext = '';
        this.fromDate = null;
        this.toDate = null;

        this.searchOption = type;
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        let searchData = {
            from: this.fromDate,
            to: this.toDate,
            username: this.searchtext,
        };
        this.agentservice.getSearchLog(searchData, pageNo)
            .subscribe((response) => {
                console.log('response search:: ', response);
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.navDetails.to = this.toDate;
                    this.navDetails.from = this.fromDate;
                    this.navDetails.param = this.searchtext;
                    this.toDate = '';
                    this.fromDate = '';
                    this.searchtext = '';
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    onGetAgentMgr(code) {
        this.navDetails.code = code;
        Cache.set('agentMgrCode', this.navDetails);
        this.router.navigate(['/sync-agentMgr']);
        this.booleans.loader = true;
    }

    onOpenModal(data) {
        this.list.data = data;
        $('#viewModal_1').modal('show');
    }
}
