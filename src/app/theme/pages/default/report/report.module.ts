import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgentManagerTransactionReportComponent } from "./agent-manager-transaction-report/agent-manager-transaction-report.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LayoutModule } from "../../../layouts/layout.module";
import { RouterModule, Routes } from "@angular/router";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgPureDatatableModule } from "ng-pure-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { DefaultComponent } from "../default.component";
import { TransactionReportComponent } from './transaction-report/transaction-report.component';
import { StateAgentCount } from "./stateAgentCount-report/stateAgentCount";
import { LgaAgentCount } from "./lgaAgentCount-report/lgaAgentCount";
import { Transactionbystateandlga } from "./transactionByStateAndLga-report/transactionbystateandlga";
import { ReportCard } from "./reportCard/reportCard";
import { AgrMgrLgaAndState } from "./AgrMgrLgaAndState-report/AgrMgrLgaAndState";
import { AgentperAgrMgr } from "./agentperAgrMgr-report/agentperAgrMgr";
import { StateAndLgaAgentsComponent } from "./stateAndLgaAgents/stateAndLgaAgents.component";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": AgentManagerTransactionReportComponent,
            },
            {
                "path": "transaction-report",
                "component": TransactionReportComponent,
            },
            {
                "path": "state-agent",
                "component": StateAndLgaAgentsComponent,
            },
            {
                "path": "state-and-lga",
                "component": Transactionbystateandlga,
            },
            {
                "path": "report",
                "component": ReportCard,
            },
            {
                "path": "institution-report",
                "component": AgrMgrLgaAndState,
            },
            {
                "path": "agents-per-agrMgr",
                "component": AgentperAgrMgr,
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        NgPureDatatableModule,
        BootstrapSwitchModule,
        RouterModule.forChild(routes),
        LayoutModule,
        NgbModule,
        PipeModule,
        AngularMultiSelectModule,
        FormsModule
    ],
    declarations: [
        AgentManagerTransactionReportComponent,
        TransactionReportComponent,
        StateAgentCount,
        LgaAgentCount,
        Transactionbystateandlga,
        ReportCard,
        AgrMgrLgaAndState,
        AgentperAgrMgr,
        StateAndLgaAgentsComponent]
})

export class ReportModule { }
