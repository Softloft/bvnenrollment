import { AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotificationService } from "../../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../../utils/cache";
import { DashboardService } from "../../../../../../services/api-service/dashboard.service";
import { BranchService } from "../../../../../../services/api-service/branch.service";
import { AgentService } from "../../../../../../services/api-service/agent.service";

declare const $;

@Component({
    selector: 'app-lgaagentcount-report',
    templateUrl: 'reportCard.html',
    styleUrls: ['reportCard.css']
})
export class ReportCard implements OnInit {
    public role = '';
    ngOnInit(): void {
        this.role = Cache.get('credentials')['user']['userType'];
    }

}
