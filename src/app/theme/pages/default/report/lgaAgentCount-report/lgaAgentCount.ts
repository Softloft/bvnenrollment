import { AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotificationService } from "../../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Cache } from "../../../../../../utils/cache";
import { DashboardService } from "../../../../../../services/api-service/dashboard.service";
import { BranchService } from "../../../../../../services/api-service/branch.service";
import { AgentService } from "../../../../../../services/api-service/agent.service";

declare const $;

@Component({
    selector: 'app-lga-agentcount-report',
    templateUrl: 'lgaAgentCount.html',
    styleUrls: ['lgaAgentCount.css']
})
export class LgaAgentCount implements OnInit {
    transactionReportdetails = [];
    public modalType = '';
    public p = 1;
    public param = '';
    public download = '';
    public totalElement = 0;
    public toDate1 = '';
    public serviceProvided = '';
    public profile = [];
    public searchOption = 'Date';
    public enrollment = [];
    public toDate = '';
    public fromDate = '';
    public fromDate1: any;
    public institutionCode = '';
    itemList = [
        { "id": 'CASH_IN', "itemName": "Cash In", "category": "Banking Services" },
        { "id": 'CASH_OUT', "itemName": "Cash Out", "category": "Banking Services" },
        { "id": 'ACCOUNT_OPENING', "itemName": "Account Opening", "category": "Banking Services" },
        { "id": 'BILLS_PAYMENT', "itemName": "Bills Payment", "category": "Banking Services" },
        { "id": 'AIRTIME_RECHARGE', "itemName": "Airtime Recharge", "category": "Banking Services" },
        { "id": 'FUNDS_TRANSFER', "itemName": "Fund Transfer", "category": "Banking Services" },
        { "id": 'BVN_ENROLMENT', "itemName": "BVN Enrollment", "category": "Banking Services" },
        { "id": 'OTHERS', "itemName": "Others", "category": "Industry Services" },
        { "id": 'ADDITIONAL_SERVICE_1', "itemName": "Additional Services 1", "category": "Industry Services" },
        { "id": 'ADDITIONAL_SERVICE_2', "itemName": "Additional Services 2", "category": "Industry Services" },
    ];
    public search = {
        to: '',
        from: ''
    };
    public id = 0;
    public booleans = {
        loader: false,
        searchStatus: false,
        loadingSearch: false
    };

    public list = {
        allContent: [],
        data: [],
    };


    constructor(private branchService: BranchService,
        private dashboardservice: DashboardService,
        private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadFunction();
        this.profile = Cache.get('credentials');
        this.enrollment = Cache.get('details');
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (this.booleans.searchStatus) {
            this.getSearchLog(this.p - 1);
        } else {
            this.getAllReport(this.p - 1);
        }
    }

    loadFunction() {
        this.getAllReport(this.p - 1);
    }

    onSelectDownType(type) {
        this.download = type;
    }

    onExport() {
        this.modalType = 'export'
        this.onOpen();
    }

    onModalSearchType() {
        this.modalType = 'search'
        this.onOpen();
    }

    getSearchLog(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.search['param'] = this.param;
        this.dashboardservice.searchReport(this.search, pageNo, 'bylga')
            .subscribe((response) => {
                this.booleans.loadingSearch = false;
                $('#CreateModal').modal('hide');
                if (response.content.length === 0) {
                    this.booleans.searchStatus = false;
                    this.notification.error('No result Found');
                } else {
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })
    }

    getAllReport(pageNo) {
        this.p = pageNo + 1;
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.dashboardservice.getStateAgentReport(pageNo, 'bylga')
            .subscribe((response) => {
                this.list.allContent = response.content;
                this.modalType = '';
                this.totalElement = response.totalElements;
                this.booleans.loader = false;
            },
            (error) => {
                this.modalType = '';
                this.booleans.loader = false;
                this.notification.error('Unable to load Enrollment', error);
            })
    }


    exportDownload() {
        this.search['to'] = this.toDate;
        this.search['from'] = this.fromDate;
        this.search['param'] = this.param;
        this.search['serviceProvided'] = this.serviceProvided;
        this.search['institutionCode'] = this.institutionCode;
        this.dashboardservice.exportReport(this.search, 'bylga')
        /*.subscribe((response) => {
                this.booleans.loadingSearch = false;
                //  window.open(response, "_blank");
                if(response.content.length === 0){
                    this.booleans.searchStatus= false;
                    this.notification.error('No result Found');
                }else{
                    this.list.allContent = response.content;
                }
            },
            error => {
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error)
            })*/
    }

    onOpen() {
        this.toDate = '';
        this.fromDate = '';
        this.param = '';
        $('#CreateModal').modal('show');
    }

    onSearch(type, count) {

    }

    onOpenModal(data) {
        this.list.data = data;
        $('#viewModal_1').modal('show');
    }

    onSearchType(type) {
        this.fromDate = null;
        this.toDate = null;
        this.param = '';
        this.searchOption = type;
    }
}
