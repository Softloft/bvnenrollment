import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { PasswordService } from "../../../services/password.service";
import { NotificationService } from "../../../services/notification.service";
import { Cache } from "../../../utils/cache";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: 'app-secure-path',
    templateUrl: './secure-path.component.html',
    styleUrls: ['./secure-path.component.css']
})
export class SecurePathComponent implements OnInit {
    changePasswordForm: FormGroup;
    loading: boolean = false;
    password = {
        passwordNew: null,
        passwordAgain: null,
        recoveryCode: null
    };
    minLength: number;
    enablePasswordInputs: boolean;
    constructor(private formBuilder: FormBuilder, public passwordService: PasswordService,
        private notificationService: NotificationService,
        private router: Router, private route: ActivatedRoute) {
        this.enablePasswordInputs = false;
        this.extractRecovery();
        this.minLength = 8;
        this.getPasswordPolicy();
    }

    ngOnInit() {
        this.initializeForm();
    }

    /**
     * method to build the reactive form.
     */
    initializeForm() {
        this.changePasswordForm = this.formBuilder.group({
            passwordNew: ['', Validators.compose([Validators.required, Validators.minLength(this.minLength)])],
            passwordAgain: ['', Validators.compose([Validators.required, Validators.minLength(this.minLength)])]
        })
    }
    changeMyPassword() {
        this.loading = true;
        this.password.recoveryCode = Cache.get('recoveryCode');
        const passwordData = {
            newPassword: this.password.passwordNew,
            recoveryCode: this.password.recoveryCode
        };
        this.passwordService.resetPassword(passwordData).subscribe((response) => {
            this.loading = false;
            console.log('Response: ', response);
            this.notificationService.success(response.responseMessage);
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 250);
        }, (error) => {
            console.log('Error: ', error);
            this.loading = false;
            this.notificationService.errorCustom(error.responseMessage || 'Unable to reset password!', error)
        })
    }
    extractRecovery() {
        const url = window.location.href;
        const recoveryCode = url.split('recoveryCode=')[1] || this.route.snapshot.paramMap.get('recoveryCode');
        if (!recoveryCode) {
            Cache.remove('recoveryCode');
            this.router.navigate(['/login']);
        }
        Cache.set('recoveryCode', recoveryCode);
        this.passwordService.validateRecoveryCode({ 'recoveryCode': recoveryCode }).subscribe((response) => {
            // console.log('response:', response)
            this.enablePasswordInputs = true;
        }, (error) => {
            // console.log('Error: ', error);
            this.notificationService.errorCustom(error.responseMessage, error);
        })
    }
    public getPasswordPolicy() {
        this.passwordService.passwordPolicy().subscribe((response) => {
            // console.log('Response: ', response)
            this.minLength = response.minimumLength | 8;
        }, (error) => {
            // console.log('Error: ', error)
        })
    }
}
