import {
    Component,
    ComponentFactoryResolver,
    OnInit,
    ViewChild,
    ViewContainerRef,
    ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { AuthenticationService } from './_services/authentication.service';
import { AlertService } from './_services/alert.service';
import { AlertComponent } from './_directives/alert.component';
import { LoginCustom } from './_helpers/login-custom';
import { Helpers } from '../helpers';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotificationService } from "../../services/notification.service";
import { AuthService } from "../../services/api-service/auth.service";
import { Cache } from "../../utils/cache";
import { PushToDashboardService } from "../../services/push-to-dashboard.service";
import { UserService } from "../../services/user.service";
import { PasswordService } from "../../services/password.service";

@Component({
    selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
    templateUrl: './templates/login-1.component.html',
    encapsulation: ViewEncapsulation.None,
})

export class AuthComponent implements OnInit {
    model: any = {};
    loading = false;
    public data = [];
    public createForm: FormGroup;
    static formData = function() {
        return {
            username: ['', Validators.compose([Validators.required])],
            password: ['', Validators.compose([Validators.required])]
        }
    };
    @ViewChild('alertSignin',
        { read: ViewContainerRef }) alertSignin: ViewContainerRef;
    @ViewChild('alertSignup',
        { read: ViewContainerRef }) alertSignup: ViewContainerRef;
    @ViewChild('alertForgotPass',
        { read: ViewContainerRef }) alertForgotPass: ViewContainerRef;

    constructor(private _router: Router,
        private authservice: AuthService,
        private notification: NotificationService,
        private _script: ScriptLoaderService,
        private userService: UserService,
        private cfr: ComponentFactoryResolver,
        private pushToDashboardService: PushToDashboardService,
        private passwordService: PasswordService,
        private fb: FormBuilder) {
        this.createForm = this.fb.group(AuthComponent.formData());
        if (Cache.get('loginStatus') === 'no') {
            this.pushToDashboardService.pushToDashboard();
        }
    }

    ngOnInit() {
        this.loading = false;
        this._script.loadScripts('body', [
            'assets/vendors/base/vendors.bundle.js',
            'assets/demo/demo3/base/scripts.bundle.js'], true).then(() => {
                Helpers.setLoading(false);
                LoginCustom.init();
            });

        window.setInterval(() => {
            // this.networkStatus = window.navigator.onLine;
            if (!navigator.onLine) {
                this.notification.error('Network disconnected, please connect back');
            }
        }, 5000);
    }

    signin() {
        this.loading = true;
        this.authservice.postLogin(this.createForm.value).subscribe(
            response => {
                this.data = response;
                Cache.set('loginStatus', 'no');
                if (this.data['userDetail']['agent']) {
                    this.data['userDetail']['agent'] = null;
                }
                if (this.data['userDetail']['branch']) {
                    this.data['userDetail']['branch'] = null;
                }
                if (this.data['userDetail']['agentManager']) {
                    this.data['userDetail']['agentManager'] = null;
                }
                if (this.data['userDetail']['agentManager']) {
                    this.data['userDetail']['agentManager'] = null;
                }
                if (this.data['userDetail']['biometrics']) {
                    this.data['userDetail']['biometrics'] = null;
                }
                const id = this.data['userDetail']['user']['role']['id'];
                this.getTask(id)
            },
            error => {
                this.loading = false;
                this.notification.error('Incorrect Credentials', error);
            });
    }

    getTask(id) {
        let allTask = [];
        this.authservice.getTask(id)
            .subscribe((response) => {
                this.userService.setAuthUser(this.data['token']);
                Cache.set('credentials', this.data['userDetail']);
                Cache.set('token', this.data['token']);
                this.passwordService.strength = 0;
                (this.data && this.data['userDetail']['user']['change_password']) ? this._router.navigate(['profile']) : this._router.navigate(['dashboard']);
                this.loading = false;
                response.forEach((val) => {
                    allTask.push(val.name);
                });
                Cache.set('tasks', allTask);
            },
            error => {
                this.loading = false;
                this.notification.error('Incorrect Credentials', error);
            })
    }

    showAlert(target) {
        this[target].clear();
        let factory = this.cfr.resolveComponentFactory(AlertComponent);
        let ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    }
}