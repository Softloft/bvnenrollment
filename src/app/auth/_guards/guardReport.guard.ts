import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Cache } from '../../../utils/cache';
import { Helpers } from "../../helpers";
import { UserService } from '../../../services/user.service';

@Injectable()
export class GuardReportGuard implements CanActivate {
    private role: any;
    constructor(private userService: UserService) {

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        this.role = Cache.get('credentials')['user']['userType'];
        if (this.role === 'NIBSS' || this.role === 'AGENT_MANAGER') {
            return true;
        } else {
            this.userService.logout();
        }

    }
}

