import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Cache } from '../../../utils/cache';
import { Helpers } from "../../helpers";
import { UserService } from '../../../services/user.service';

@Injectable()
export class CheckPasswordGuard implements CanActivate {
    constructor(private userService: UserService) {

    }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if ((sessionStorage.getItem('credentials') != 'null' && sessionStorage.getItem('credentials') != undefined)
            && (sessionStorage.getItem('bvn-auth-token') != 'null' && sessionStorage.getItem('bvn-auth-token') != undefined)) {
            return this.checkUserStatus(state.url);
        } else {
            this.userService.logout();
        }

    }

    checkUserStatus(url) {
        const userData = Cache.get('credentials');
        // console.log('userData', userData);
        if (userData && userData['user']['change_password']) {
            if (this.checkUserRoute(url)) { return true; }
        } else {
            return true;
        }
        return false;
    }

    checkUserRoute(url) {
        Helpers.setLoading(false);
        return (url === '/profile');

        /* if (url === '/profile'){
           return true;
         }
         return false;
         */
    }
}

