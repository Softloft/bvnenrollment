import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Rx";
import { Cache } from "../../../utils/cache";
import { UserService } from "../../../services/user.service";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private _router: Router, private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        if (this.userService.isLoggedIn()) {
            return true;
        } else {
            this._router.navigate(['login']);
            return false;
        }
    }
}