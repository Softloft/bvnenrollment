import { Injectable } from '@angular/core';
import { UserService } from "../../../services/user.service";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";

@Injectable()
export class RoleGuardService {

    constructor(private userService: UserService,
        private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.checkUserRole();
    }

    checkUserRole() {
        const user = this.userService.getAuthUser()['userDetail']['user']['userType'];
        console.log('user :: ', user);
        /*if(user === 'NIBSS'){
            this.router.navigate(['/dashboard']);
        } else if(user === 'AGENT_MGR'){
            this.router.navigate(['/agent-mgr']);
        } else if(user === 'AGENT'){
            this.router.navigate(['/agent']);
        } else if(user === 'ENROLLER'){
            this.router.navigate(['/enroller']);
        }*/
        return true
    }

}
