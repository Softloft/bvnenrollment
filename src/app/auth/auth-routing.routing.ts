import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth.component';
import { SecurePathComponent } from "./secure-path/secure-path.component";

const routes: Routes = [
    { path: '', component: AuthComponent },
    { path: 'verifyRecoveryCode', component: SecurePathComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {
}