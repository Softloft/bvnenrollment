import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { PasswordService } from "../../../../services/password.service";
import { NotificationService } from "../../../../services/notification.service";
import { defaultHostName } from "../../../../utils/check-hostname";

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
    private default = defaultHostName();
    resetPassword = {
        email: null,
        path: null
    };
    resetPasswordForm: FormGroup;
    public EMAIL_VALIDATION: any = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    loading: boolean;
    constructor(private formBuilder: FormBuilder, private passwordService: PasswordService,
        private notificationService: NotificationService) { }

    ngOnInit() {
        this.loading = false;
        this.initializeForm();
    }
    /**
     * method to build the reactive form.
     */
    public initializeForm() {
        this.resetPasswordForm = this.formBuilder.group({
            email: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.pattern(this.EMAIL_VALIDATION)])]
        });
    }
    startPasswordReset() {
        this.loading = true;
        const userLink = window.location;
        this.resetPassword.path = userLink.origin + '/verifyRecoveryCode?recoveryCode=';
        this.passwordService.sendResetLink(this.resetPassword).subscribe((response) => {
            this.loading = false;
            // console.log('Response: ', response);
            this.notificationService.success(response.responseMessage || 'Please check your inbox')
        }, (error) => {
            this.loading = false;
            this.notificationService.errorCustom(error.responseMessage || 'Unable to reset password!', error)
        })
    }
}
