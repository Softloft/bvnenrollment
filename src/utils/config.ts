import { UserService } from '../services/user.service';
import { DateFormatting } from './util';
import { environment } from '../environments/environment';
import { Cache } from './cache';
import { HttpHeaders } from "@angular/common/http";
import { defaultHostName } from "./check-hostname";


/**
 *  Environment type configuration
 */


export class ApiConfig extends DateFormatting {

    static API_DEFAULT_URL = defaultHostName();
    static API_KEY = environment.API_KEY;
    protected headers = { headers: this.setHeaders() };
    protected authToken: any;

    constructor(private myUserService: UserService) {
        super();
        console.log('router :: ', window.location.hostname)
    }

    /**
     * This is used to Set HttpHeaders on before request
     * @returns {HttpHeaders}
     */
    protected setHeaders(): HttpHeaders {
        this.authToken = this.myUserService.getAuthUser();

        const headersConfig = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        };

        if (this.myUserService.isLoggedIn()) {
            headersConfig['Authorization'] = `${Cache.get('token')}`;
        }
        if (ApiConfig.API_KEY) {
            headersConfig['API-KEY'] = ApiConfig.API_KEY;
        }
        // if (ApiConfig.API_KEY) {
        //   headersConfig['API-KEY'] = ApiConfig.API_KEY;
        // }
        return new HttpHeaders(headersConfig);
    }


}

