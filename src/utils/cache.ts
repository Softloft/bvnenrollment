import { EncryptionService } from '../services/encryption.service';
export class Cache {

    /**
     * Set Data to session storage
     * @param key
     * @param data
     */
    static set(key: string, data: any) {
        // console.log('data=', data);
        const encryptedData = EncryptionService.encrypt(data);
        sessionStorage.setItem(key, encryptedData);
    }

    /**
     * get data from session storage
     * @param key
     * @returns {any}
     */
    static get(key: string) {
        if (sessionStorage.getItem(key) == 'null' || sessionStorage.getItem(key) == 'undefined' || !sessionStorage.getItem(key)) {
            sessionStorage.removeItem(key);
            localStorage.removeItem(key);
            return false;
        } else {
            if (key === 'bvn-auth-token' && sessionStorage.getItem('bvn-auth-token') != 'null') {
                const user = EncryptionService.decrypt(sessionStorage.getItem(key)); // EncryptionService.jwtDecrypt(EncryptionService.decrypt(sessionStorage.getItem(key)));
                if (user['user_type'] === 'App\\MerchantUser') {
                    user['user']['id'] = user['user']['merchant']['id'];
                    user['user']['enabled'] = user['user']['merchant']['enabled'];
                }
                return user;
            }
            return EncryptionService.decrypt(sessionStorage.getItem(key));
        }
    }

    /**
     * Used to clear cache Data
     */
    static clear() {
        sessionStorage.clear();
    }

    /**
     * This is used to remove a data by key
     * @param key
     */
    static remove(key) {
        sessionStorage.removeItem(key);
    }
    constructor() { }

}
