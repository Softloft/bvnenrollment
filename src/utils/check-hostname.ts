import { environment } from "../environments/environment";

export const defaultHostName = () => {
    if (window.location.hostname === '197.255.63.146') {
        return environment.API_URL.default3;
    } else if (window.location.hostname === 'localhost') {
        return environment.API_URL.default4;
    } else if (window.location.hostname === '196.6.103.10') {
        return environment.API_URL.default2;
    } else if (window.location.hostname === '196.6.103.30') {
        return environment.API_URL.default5;
    } else if (window.location.hostname === 'agencyservices.com.ng') {
        return environment.API_URL.default1;
    } else {
        return environment.API_URL.default1;
    }
}