const jetpack = require('fs-jetpack');
const path = require('path');
const minifier = require('minifier');

/**
 * Do actual minification
 * @param file
 * @param folder
 */
var processMinifier = function (file, folder) {
    if (/.*(\.js|\.css)$/g.test(file)) {
        console.info('start minification for ' + file);
        const filePath = path.join(__dirname, 'dist/assets/' + folder, file);
        minifier.minify(filePath, {output: filePath});
    }
};

var indepthFileProcess = function(newPath, checkFolder) {
    const currentPath = newPath + checkFolder;
    console.log({currentPath: currentPath});
    var file = '';
    const files = jetpack.list(path.join(__dirname, 'dist/assets/' + currentPath));
    console.log('files=', files);
    for (var j = 0; j < files.length; j++) {
        file = files[j];
        if (file.indexOf('.min.') > -1) {
            continue;
        }
        if (file.indexOf('.') === -1) {
            console.log('newFolder=', file);
            indepthFileProcess(currentPath + '/', file);
        } else {
            processMinifier(file, currentPath);
        }
    }
};

/**
 * Start file in assets minification
 */
function runMinification() {

    const fileFolders = jetpack.list(path.join(__dirname, 'dist/assets'));

// console.info(fileFolders);

    for (var i = 0; i < fileFolders.length; i++) {
        var checkFolder = /.*(\.)$/g.test(fileFolders[i]) ? '' : fileFolders[i];
        var file = '';
        if (checkFolder === '') {
            file = fileFolders[i];
            if (file.indexOf('.min.') > -1) {
                continue;
            }
            processMinifier(file, checkFolder);
        } else {
            if (checkFolder.indexOf('.') === -1) {
                indepthFileProcess('', checkFolder);
            }
        }
    }

    console.info('Minification for assets css/js done ');
}

console.info('Asset css/js minification will start in 10 seconds, please wait');

setTimeout(function () {
    runMinification();
}, 10000);